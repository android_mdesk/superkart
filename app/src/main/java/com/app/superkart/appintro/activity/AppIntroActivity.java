package com.app.superkart.appintro.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.superkart.R;
import com.app.superkart.appintro.fragment.AppIntroFragment1;
import com.app.superkart.appintro.fragment.AppIntroFragment2;
import com.app.superkart.appintro.fragment.AppIntroFragment3;
import com.app.superkart.appintro.fragment.AppIntroFragment4;
import com.app.superkart.appintro.fragment.AppIntroFragment5;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.MainActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.GPSTracker;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AppIntroActivity extends AppCompatActivity implements View.OnClickListener {
    public static ViewPager viewpager;
    private Context mContext;
    public static TabLayout tabLayout;
    private ArrayList<Fragment> mFragments;
    private AppIntroPagerAdapter mSectionsPagerAdapter;
    public LinearLayout li_next, li_bottom;
    final static int REQUEST_LOCATION = 199;
    private GoogleApiClient googleApiClient;
    String latitude, longitude, address, city, state, country, postalCode;
    List<Address> addresses = new ArrayList<>();
    private final int REQUEST_LOCATION_PERMISSION = 1;
    private FusedLocationProviderClient mFusedLocationClient;
    PreferenceManager preferenceManager;
    public boolean is_checked = false;

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mContext = this;
        Utils.showTitle(window, mContext);
        setContentView(R.layout.app_intro_activity);
        Log.e("AppIntroActivity", "AppIntroActivity");
        preferenceManager = new PreferenceManager(this);
        init();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
            }
        }

        mSectionsPagerAdapter = new AppIntroPagerAdapter(getSupportFragmentManager());
        mFragments = new ArrayList<>(Arrays.asList(new AppIntroFragment1(), new AppIntroFragment2(), new AppIntroFragment3(), new AppIntroFragment4(), new AppIntroFragment5()));
        mSectionsPagerAdapter.setFragments(mFragments);
        viewpager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(viewpager, false);

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Todo Location Already on  ... end


        Enable_Location();
       /* if (!hasGPSDevice(this)) {
            Toast.makeText(getApplicationContext(), "Gps not Supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && hasGPSDevice(this)) {
            // Log.e("Neha","Gps already enabled");
            Toast.makeText(getApplicationContext(), "Gps not enabled", Toast.LENGTH_SHORT).show();

//            openDialogFunction();

            enableLoc();
        }*/

    }

    public void init() {
        viewpager = findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        li_bottom = findViewById(R.id.li_bottom);
        li_next = findViewById(R.id.li_next);
        li_next.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_next:
                is_checked = true;
                SharedPreferences pref = getSharedPreferences("intro_check", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("is_checked", is_checked);
                editor.commit();
                AppConstant.check_without_social = "0";
                Intent i = new Intent(AppIntroActivity.this, BaseActivity.class);
                startActivity(i);
                break;
        }
    }

    public class AppIntroPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragments;

        public AppIntroPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            int j = viewpager.getCurrentItem();
            if (j == 4) {
                li_bottom.setVisibility(View.INVISIBLE);
            } else {
                li_bottom.setVisibility(View.VISIBLE);
            }
            return mFragments.size();
        }

        public void setFragments(List<Fragment> fragments) {
            this.mFragments = fragments;
        }
    }

    private boolean hasGPSDevice(Context context) {
        final LocationManager mgr = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void enableLoc() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                            Log.e("connected", "connected");
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            Log.e("suspended", "suspended");
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                            Log.e("Location error", "Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(7 * 1000);  //30 * 1000
            locationRequest.setFastestInterval(5 * 1000); //5 * 1000
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {

                    Log.e("result", "result");
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.e("RESOLUTION_REQUIRED", "");
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(AppIntroActivity.this, REQUEST_LOCATION);

                                // getActivity().finish();
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                                Log.e("error", "error");
                            }
                            break;
                    }
                }
            });
        }
    }

    public boolean Enable_Location() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(myIntent, 10);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Log.e("PER", "Location");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {/* ... */}
                }).check();


        return false;
    }
}
