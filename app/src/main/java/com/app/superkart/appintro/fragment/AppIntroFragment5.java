package com.app.superkart.appintro.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.superkart.R;
import com.app.superkart.appintro.activity.AppIntroActivity;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.AppConstant;
import com.google.android.material.tabs.TabLayout;

import static android.content.Context.MODE_PRIVATE;

public class AppIntroFragment5 extends Fragment {
    private TextView btn_login;
    private TabLayout tabLayout;
    private TextView txt_skip;
    public boolean is_checked = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_app_intro5, container, false);
        Log.e("AppIntroFragment5", "AppIntroFragment5");
        init(rootview);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_checked = true;
                SharedPreferences pref = getActivity().getSharedPreferences("intro_check", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("is_checked", is_checked);
                editor.commit();
                AppConstant.check_without_social="0";
                Intent i = new Intent(getActivity(), BaseActivity.class);
                startActivity(i);
            }
        });

        txt_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), BaseActivity.class);
                startActivity(i);
            }
        });
        return rootview;

    }

    public void init(View rootview) {
        btn_login = rootview.findViewById(R.id.btn_login);
        tabLayout = rootview.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(AppIntroActivity.viewpager, true);
        txt_skip = rootview.findViewById(R.id.txt_skip);

    }

    @Override
    public void onResume() {

        super.onResume();
    }
}
