package com.app.superkart.appintro.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.app.superkart.R;
import com.app.superkart.ui.activity.Signup.LoginActivity;

public class AppIntroFragment2 extends Fragment implements View.OnClickListener {
    private TextView txt_skip;
    private ImageView img_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_app_intro2, container, false);
        Log.e("AppIntroFragment2", "AppIntroFragment2");
        init(rootview);
        txt_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
            }
        });
        return rootview;
    }

    public void init(View rootview) {
        txt_skip = rootview.findViewById(R.id.txt_skip);
        img_back = rootview.findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onResume() {

        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:

                break;
        }
    }
}
