package com.app.superkart.ui.activity.Service;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Service.Home_Service_Detail_ListAdpter;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class Service_Detail_Activity extends AppCompatActivity implements View.OnClickListener {
    private Context context;
    private ImageView img_back;
    private RecyclerView recycle_service;
    private GridLayoutManager gridLayoutManager;
    private ArrayList<HomeCategoryModel> arrayList_service = new ArrayList<>();
    private Home_Service_Detail_ListAdpter home_service_detail_listAdpter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        Utils.showTitle(window,context);
        setContentView(R.layout.home_service_detail);
        Log.e("Service_Detail_Activity", "Service_Detail_Activity");
        init();
        bindServiceData();
    }

    public void init() {
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    public void bindServiceData() {

        arrayList_service.clear();
        HomeCategoryModel homeCategoryModel;

        homeCategoryModel = new HomeCategoryModel(R.drawable.basin, "Basin & Sink");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.bath, "Bath Fitting");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.blockage, "Blockage");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.tap, "Tap & Mixer");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.toilet, "Toilet");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.tank, "Water Tank");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.motor, "Motor");
        arrayList_service.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel(R.drawable.bath, "Minor Installation");
        arrayList_service.add(homeCategoryModel);

        recycle_service = findViewById(R.id.recycle_service);

        gridLayoutManager = new GridLayoutManager(Service_Detail_Activity.this, 3);
        recycle_service.setLayoutManager(gridLayoutManager);
        home_service_detail_listAdpter = new Home_Service_Detail_ListAdpter(Service_Detail_Activity.this, arrayList_service);
        recycle_service.setAdapter(home_service_detail_listAdpter);
    }
}
