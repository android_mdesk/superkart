package com.app.superkart.ui.activity.Signup;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.superkart.R;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.facebook.BuildConfig;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private ImageView img_back;
    private LinearLayout li_back;
    private TextView btn_signup, btn_fb, btn_google;
    private Context mcontext;
    private CallbackManager callbackManager;
    String f_image = "", fb_id = "", fb_username = "", fbemail = "";
    private ProgressDialog progressDialog;
    private static final int RC_SIGN_IN = 1;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInOptions gso;
    private String googleid = "", googleemail = "", googleusername = "", googleImage = "";
    private EditText et_email, et_password, et_phone;
    public String str_email = "", str_password = "", str_phone = "", str_convert_image = "";
    public PreferenceManager preferenceManager;
    private SharedPreferences preferences;
    private SharedPreferences preferences_token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        Log.e("SignUpActivity", "SignUpActivity");
        mcontext = this;
        Window window = this.getWindow();
        Utils.showTitle1(window, mcontext);
        preferenceManager = new PreferenceManager(this);
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        preferences_token = getSharedPreferences("X", Context.MODE_PRIVATE);
        FacebookSdk.setApplicationId(getString(R.string.facebook_app_id));
        //initialize Facebook SDK
        FacebookSdk.sdkInitialize(getApplicationContext());
        if (BuildConfig.DEBUG) {
            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        }
        init();
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
    }

    public void init() {
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_phone = findViewById(R.id.et_phone);
        img_back = findViewById(R.id.img_back);
        li_back = findViewById(R.id.li_back);
        btn_signup = findViewById(R.id.btn_signup);
        btn_fb = findViewById(R.id.btn_fb);
        btn_google = findViewById(R.id.btn_google);
        li_back.setOnClickListener(this);
        btn_signup.setOnClickListener(this);
        btn_fb.setOnClickListener(this);
        btn_google.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_back:
                Utils.hideKeyboard(SignUpActivity.this);
                finish();
                break;
            case R.id.btn_signup:
                AppConstant.check_without_social = "0";
                AppConstant.check_sign_up = "simple";
                Utils.hideKeyboard(SignUpActivity.this);
                str_email = et_email.getText().toString().trim();
                str_password = et_password.getText().toString().trim();
                str_phone = et_phone.getText().toString().trim();

                if (checkValidations1(v)) {
                    if (Utils.isNetworkAvailable(SignUpActivity.this)) {
                        Register(v);
                    } else {
                        Utils.showSnackBar(v, getResources().getString(R.string.check_internet));
                    }
                }
                break;
            case R.id.btn_fb:
                AppConstant.check_without_social = "1";
                AppConstant.check_sign_up = "fb";
                facebooklogin(v);
                break;
            case R.id.btn_google:
                AppConstant.check_without_social = "1";
                AppConstant.check_sign_up = "google";
                onSignInClicked();
                break;
        }
    }

    public void Register(View v) {
        ProgressDialog progressDialog_main = new ProgressDialog(SignUpActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.Register;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(SignUpActivity.this);
                            JSONObject obj_main = new JSONObject(response);
                            Log.e("Sign up response", response);
                            if (obj_main.optString("status").equals("1")) {
                                JSONObject obj_result = obj_main.optJSONObject("result");
                                SharedPreferences preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(PreferenceManager.user_id, obj_result.optString("user_id"));
                                editor.putString(PreferenceManager.email_id, obj_result.optString("email"));
                                editor.putString(PreferenceManager.username, obj_result.optString("username"));
                                editor.putString(PreferenceManager.phone_number, obj_result.optString("mobile"));
                                editor.putString(PreferenceManager.token_in_return, obj_result.optString("token"));
                                editor.putString(PreferenceManager.profile_pic, obj_result.optString("profile_pic"));
                                editor.commit();
                                Toast.makeText(mcontext, obj_main.optString("msg"), Toast.LENGTH_LONG).show();
                                et_email.setText("");
                                et_password.setText("");
                                et_phone.setText("");
                                if (AppConstant.check_sign_up.equals("fb") || AppConstant.check_sign_up.equals("google")) {
                                    Intent i = new Intent(mcontext, BaseActivity.class);
                                    startActivity(i);
                                } else {
                                    Intent i = new Intent(mcontext, OTPActivity.class);
                                    startActivity(i);
                                }

                            } else {
                                Utils.showSnackBar(v, obj_main.optString("msg"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBar(v, message);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                // params.put("Content-Type", "application/json");
                params.put("Content-Type", "application/x-www-form-urlencoded");
                // params.put("Content-Type", "application/form-data");

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (AppConstant.check_sign_up.equals("simple")) {
                    params.put("password", str_password);
                    params.put("mobile", str_phone);
                } else if (AppConstant.check_sign_up.equals("fb")) {
                    params.put("facebook_id", fb_id);
                    params.put("username", fb_username);
                    params.put("profile_pic", f_image);
                } else {
                    params.put("google_id", googleid);
                    params.put("username", googleusername);
                    params.put("profile_pic", googleImage);
                }
                params.put("email", str_email);
                params.put("device_id", preferences_token.getString(PreferenceManager.DeivceToken, ""));
                params.put("regi_from", Constants.regi_from);
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_email.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.pleaseenteremail));
            result = false;
        } else if (!Utils.emailValidator(et_email.getText().toString())) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertvalidemail));
            result = false;
        } else if (et_password.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertpassword));
            result = false;
        } else if (et_phone.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertphone));
            result = false;
        } else if (et_phone.getText().toString().length() < 10) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertphone10));
            result = false;
        }
        return result;
    }

    private void facebooklogin(View v) {

        if (Utils.isNetworkAvailable(SignUpActivity.this)) {

            callbackManager = CallbackManager.Factory.create();
            LoginManager manager = LoginManager.getInstance();
            Log.e("callbackManager", "callbackManager");
            manager.setLoginBehavior(LoginBehavior.WEB_ONLY);
            manager.logInWithReadPermissions(SignUpActivity.this, Arrays.asList("public_profile", "user_about_me", "email", "user_birthday"));
            LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult result) {
                    // TODO Auto-generated method stub
                    System.out.println("onSuccess");
                    progressDialog = new ProgressDialog(SignUpActivity.this);
                    progressDialog.setMessage("Processing datas...");
                    progressDialog.show();
                    String accessToken = result.getAccessToken().getToken();
                    GraphRequest request = GraphRequest.newMeRequest(result.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                        @Override
                        public void onCompleted(org.json.JSONObject object, GraphResponse response) {
                            try {
                                Log.e("response", "" + response);
                                fb_id = object.getString("id");
                                fb_username = object.getString("name");
                                fbemail = object.getString("email");
                                str_email = object.getString("email");
                                f_image = "http://graph.facebook.com/" + fb_id + "/picture?type=large";
                                Register(v);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                    progressDialog.dismiss();
                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id,name,email,first_name,last_name,gender");
                    request.setParameters(parameters);
                    request.executeAsync();
                }

                @Override
                public void onCancel() {
                    //Toast.makeText(LoginActivity.this, "Cancel", Toast.LENGTH_LONG).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Toast.makeText(SignUpActivity.this, "onError", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(SignUpActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void onSignInClicked() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.e("idToken", "" + result.getSignInAccount());
            View v = null;
            handleSignInResult(result, v);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result, View v) {

        if (result.isSuccess()) {
            Log.e("isSuccess", "" + result.isSuccess());
            GoogleSignInAccount acct = result.getSignInAccount();
            Uri image = acct.getPhotoUrl();
            googleid = acct.getId();
            googleemail = acct.getEmail();
            googleusername = acct.getDisplayName();
            googleImage = String.valueOf(image);
            str_email = acct.getEmail();
            Register(v);
        } else {
            Log.e("Not isSuccess", "" + result.isSuccess());
        }
    }

    @Override
    protected void onResume() {
        Utils.hideKeyboard(SignUpActivity.this);
        super.onResume();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}