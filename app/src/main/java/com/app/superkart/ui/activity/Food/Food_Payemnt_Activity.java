package com.app.superkart.ui.activity.Food;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.Payment_Card_adpter;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class Food_Payemnt_Activity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private ImageView img_back;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> arrayList = new ArrayList<>();
    private Payment_Card_adpter payment_card_adpter;
    private TextView txt_btn_pay;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        Utils.showTitle(window, context);
        setContentView(R.layout.food_payment_layout);
        Log.e("Food_Payemnt_Activity", "Food_Payemnt_Activity");
        init();
        for (int i = 0; i < 2; i++) {
            arrayList.add("a");
        }
        payment_card_adpter = new Payment_Card_adpter(Food_Payemnt_Activity.this, arrayList);
        recycle_item.setAdapter(payment_card_adpter);
    }

    public void init() {

        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(Food_Payemnt_Activity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);

        txt_btn_pay = findViewById(R.id.txt_btn_pay);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        txt_btn_pay.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_btn_pay:
                OrderConfirm_Dailog();
                break;
        }
    }

    public void OrderConfirm_Dailog() {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_dailog_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView btn_home = (TextView) dialog.findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // change.onhome();
                Intent intent = new Intent(Food_Payemnt_Activity.this, BaseActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }
}

