package com.app.superkart.ui.activity.Signup;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.superkart.R;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity extends Activity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private LinearLayout li_back;
    private TextView btn_save;
    private EditText et_email, et_password, et_confirmPass;
    public String str_email = "", str_password = "", str_confirm_pass = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_layout);
        Log.e("ForgotPasswordActivity", "ForgotPasswordActivity");
        mcontext = this;
        Window window = this.getWindow();
        Utils.showTitle1(window, mcontext);
        init();
    }

    public void init() {
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_confirmPass = findViewById(R.id.et_confirmPass);
        img_back = findViewById(R.id.img_back);
        li_back = findViewById(R.id.li_back);
        btn_save = findViewById(R.id.btn_save);
        img_back.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        li_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_back:
                Utils.hideKeyboard(ForgotPasswordActivity.this);
                finish();
                break;
            case R.id.btn_save:
                Utils.hideKeyboard(ForgotPasswordActivity.this);
                str_email = et_email.getText().toString().trim();
                str_password = et_password.getText().toString().trim();
                str_confirm_pass = et_confirmPass.getText().toString().trim();
                if (checkValidations1(v)) {
                    if (Utils.isNetworkAvailable(ForgotPasswordActivity.this)) {
                        forgotPassword(v, str_email, str_password, str_confirm_pass);
                    } else {
                        Utils.showSnackBar(v, getResources().getString(R.string.check_internet));
                    }
                }

                break;
        }
    }


    public void forgotPassword(View v, String email, String password, String CPassword) {
        ProgressDialog progressDialog_main = new ProgressDialog(ForgotPasswordActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.forgotPassword;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(ForgotPasswordActivity.this);
                            JSONObject obj_main = new JSONObject(response);
                            Log.e("response", response);
                            if (obj_main.optString("status").equals("1")) {
                                JSONObject obj_result = obj_main.getJSONObject("result");
                                et_email.setText("");
                                et_password.setText("");
                                et_confirmPass.setText("");
                            } else {
                                Utils.showSnackBar(v, obj_main.optString("msg"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBar(v, message);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //params.put("Content-Type", "multipart/form-data");
                //  params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("confrim_password", CPassword);
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_email.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.pleaseenteremail));
            result = false;
        } else if (!Utils.emailValidator(et_email.getText().toString())) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertvalidemail));
            result = false;
        } else if (et_password.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertpassword));
            result = false;
        } else if (et_confirmPass.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertpasswordConfirm));
            result = false;
        } else if (!et_password.getText().toString().equals(et_confirmPass.getText().toString())) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertpasswordmis));
            result = false;
        }
        return result;
    }

    @Override
    protected void onResume() {
        Utils.hideKeyboard(ForgotPasswordActivity.this);
        super.onResume();
    }
}
