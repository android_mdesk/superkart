package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.Fashion_Address_Item_Adpter;
import com.app.superkart.model.Shopping.ProductListModel;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Fashion_Addresss_Activity extends AppCompatActivity implements View.OnClickListener {
    private Context context;
    private ImageView img_back;
    private TextView txt_btn_continue, txt_btn_address, txt_total_MRP, txt_discount, txt_shipp_charge, txt_grandTotal, txt_amount;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ProductListModel> arrayList = new ArrayList<>();
    private Fashion_Address_Item_Adpter fashion_address_item_adpter;
    private TextView txt_userName, txt_address, txt_phone;
    private SharedPreferences preferences;
    private SharedPreferences pref;
    private ProgressDialog progressDialog;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        Utils.showTitle(window, context);
        setContentView(R.layout.fashion_address_layout);
        Log.e("Fashion_Addresss_Activity", "Fashion_Addresss_Activity");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        pref = getSharedPreferences("ShipingAddress", MODE_PRIVATE);
        init();

        txt_userName.setText(AppConstant.name + " (Default)");
        txt_address.setText(AppConstant.Addressline1 + "," + AppConstant.Addressline2 + "\n" + AppConstant.city + "," + AppConstant.state_address + "," + "\n" + AppConstant.zip);
        txt_phone.setText("Mobile: " + AppConstant.phone);

        if (Utils.isNetworkAvailable(Fashion_Addresss_Activity.this)) {
            revieworder();
        } else {
            Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
    }

    public void init() {
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(Fashion_Addresss_Activity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);

        img_back = findViewById(R.id.img_back);
        txt_userName = findViewById(R.id.txt_userName);
        txt_address = findViewById(R.id.txt_address);
        txt_phone = findViewById(R.id.txt_phone);
        txt_btn_continue = findViewById(R.id.txt_btn_continue);
        txt_btn_address = findViewById(R.id.txt_btn_address);
        txt_total_MRP = findViewById(R.id.txt_total_MRP);
        txt_discount = findViewById(R.id.txt_discount);
        txt_amount = findViewById(R.id.txt_amount);
        txt_shipp_charge = findViewById(R.id.txt_shipp_charge);
        txt_grandTotal = findViewById(R.id.txt_grandTotal);
        img_back.setOnClickListener(this);
        txt_btn_continue.setOnClickListener(this);
        txt_btn_address.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_btn_continue:

                break;
            case R.id.txt_btn_address:
                Intent intent = new Intent(Fashion_Addresss_Activity.this, AddressListActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void revieworder() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.revieworder, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    arrayList.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            ProductListModel model = new ProductListModel();
                            model.setProduct_image(array_result.optJSONObject(i).optString("productimage"));
                            model.setProduct_name(array_result.optJSONObject(i).optString("product_name"));
                            model.setQty(array_result.optJSONObject(i).optString("qty"));
                            model.setSale_price(array_result.optJSONObject(i).optString("price"));
                            arrayList.add(model);
                        }
                        if (arrayList.size() > 0) {
                            recycle_item.setVisibility(View.VISIBLE);
                            fashion_address_item_adpter = new Fashion_Address_Item_Adpter(Fashion_Addresss_Activity.this, arrayList);
                            recycle_item.setAdapter(fashion_address_item_adpter);
                        } else {
                            recycle_item.setVisibility(View.GONE);
                        }
                        if (json_main.optString("grandtotal").equals("") || json_main.optString("grandtotal").equals("null") || json_main.optString("grandtotal").equals(null)) {
                            txt_total_MRP.setText("₹ 0.0");
                            txt_grandTotal.setText("₹ 0.0");
                            txt_amount.setText("₹ 0.0");
                        } else {
                            txt_total_MRP.setText("₹" + json_main.optString("grandtotal"));
                            txt_grandTotal.setText("₹" + json_main.optString("grandtotal"));
                            txt_amount.setText("₹" + json_main.optString("grandtotal"));
                            AppConstant.amount = json_main.optString("grandtotal");
                        }

                        JSONObject obj_cashfree = json_main.optJSONObject("cashfree");
                        AppConstant.secretKey = obj_cashfree.optString("secretKey");
                        AppConstant.appId = obj_cashfree.optString("appId");
                        AppConstant.payment_mode_cashfree_url = obj_cashfree.optString("endpoint_url");

                        if (json_main.optString("chargeship").equals("") || json_main.optString("chargeship").equals("null") || json_main.optString("chargeship").equals(null)) {
                            txt_shipp_charge.setText("₹ 0.0");
                        } else {
                            txt_shipp_charge.setText("₹" + json_main.optString("chargeship"));
                        }
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("city", "Anantnag");
                Log.e("params", "" + Constants.revieworder + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.revieworder);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


}
