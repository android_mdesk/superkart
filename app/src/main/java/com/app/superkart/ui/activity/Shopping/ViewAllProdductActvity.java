package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.ProductListModel;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewAllProdductActvity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recycle_list, recycle_lookingfor;
    private Context context;
    private ArrayList<ProductListModel> arrayList_item = new ArrayList<>();
    private ArrayList<SellerForyouModel> arrayList_lookingfor = new ArrayList<>();
    private CategoryListAdpter categoryListAdpter;
    private LookingForAdpter lookingForAdpter;
    private ImageView img_back;
    private ImageView img_btn_whishlist, img_search;
    private TextView txt_offer_title, txt_new, txt_Trending;
    private ProgressDialog progressDialog;
    public String offer_id = "", offer_title = "";
    private SharedPreferences preferences;
    private LinearLayout li_product_search;
    private EditText et_searchh;
    private SharedPreferences pref_city;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        Utils.showTitle1(window, context);
        setContentView(R.layout.home_all_sub_category_list);
        Log.e("ViewAllProdductActvity", "ViewAllProdductActvity");
        init();
        intentData();
        preferences = getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);
        pref_city = getSharedPreferences("city", Context.MODE_PRIVATE);
        Log.e("user_id", "" + preferences.getString(PreferenceManager.user_id, ""));

        et_searchh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    if (arrayList_item.size() > 0) {
                        categoryListAdpter.getFilter().filter(s);
                    }

                } else {
                    if (arrayList_item.size() > 0) {
                        categoryListAdpter.resetData();
                        categoryListAdpter.notifyDataSetChanged();
                        li_product_search.setVisibility(View.GONE);
                        Utils.hideKeyboard(ViewAllProdductActvity.this);
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void init() {
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        recycle_lookingfor = findViewById(R.id.recycle_lookingfor);
        img_back = findViewById(R.id.img_back);
        txt_offer_title = findViewById(R.id.txt_offer_title);
        img_btn_whishlist = findViewById(R.id.img_btn_whishlist);
        img_search = findViewById(R.id.img_search);
        li_product_search = findViewById(R.id.li_product_search);
        et_searchh = findViewById(R.id.et_search);
        img_back.setOnClickListener(this);
        img_btn_whishlist.setOnClickListener(this);
        img_search.setOnClickListener(this);
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            offer_id = (String) bd.get("offer_id");
            offer_title = (String) bd.get("offer_title");
            Log.e("offer_id", "" + offer_id);
            if (offer_title.equals("") || offer_title.equals(null) || offer_title.equals("null")) {

            } else {
                txt_offer_title.setText(offer_title);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                li_product_search.setVisibility(View.GONE);
                finish();
                break;
            case R.id.img_btn_whishlist:
                li_product_search.setVisibility(View.GONE);
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                } else {
                    Intent i = new Intent(context, WhishListActivity.class);
                    startActivity(i);
                }
                break;
            case R.id.img_search:
                li_product_search.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void offerproductlist() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.offerproductlist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    arrayList_lookingfor.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            SellerForyouModel model = new SellerForyouModel();
                            model.setProduct_name(array_result.optJSONObject(i).optString("product"));
                            model.setItem_array(array_result.optJSONObject(i).optString("allitems"));
                            arrayList_lookingfor.add(model);
                        }
                        if (arrayList_lookingfor.size() > 0) {
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ViewAllProdductActvity.this, RecyclerView.HORIZONTAL, false);
                            recycle_lookingfor.setLayoutManager(linearLayoutManager);
                            lookingForAdpter = new LookingForAdpter(ViewAllProdductActvity.this, arrayList_lookingfor);
                            recycle_lookingfor.setAdapter(lookingForAdpter);
                        }
                    } else {
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("offer_id", offer_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                // params.put("city", pref_city.getString("city", ""));
                Log.e("params", "" + Constants.offerproductlist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.offerproductlist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    public class LookingForAdpter extends RecyclerView.Adapter<LookingForAdpter.MyViewHolder> {

        private List<SellerForyouModel> arrayList;
        private Context context;
        private int pos = 0;


        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txt_shopping;

            public MyViewHolder(View view) {
                super(view);
                txt_shopping = view.findViewById(R.id.txt_shopping);
            }
        }

        public LookingForAdpter(Context context, List<SellerForyouModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public LookingForAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.looking_for_adpter, parent, false);

            return new LookingForAdpter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(LookingForAdpter.MyViewHolder holder, int position) {
            SellerForyouModel model = arrayList.get(position);

            if (model.getProduct_name().equals("")) {

            } else {
                holder.txt_shopping.setText(model.getProduct_name());
            }

            if (pos == position) {
                try {
                    arrayList_item.clear();
                    JSONArray array = new JSONArray(model.getItem_array());
                    for (int i = 0; i < array.length(); i++) {
                        ProductListModel productListModel = new ProductListModel();
                        productListModel.setProduct_id(array.optJSONObject(i).optString("product_id"));
                        productListModel.setIs_fav(array.optJSONObject(i).optString("is_fav"));
                        productListModel.setProduct_name(array.optJSONObject(i).optString("product_name"));
                        productListModel.setProduct_image(array.optJSONObject(i).optString("product_image"));
                        productListModel.setRegular_price(array.optJSONObject(i).optString("regular_price"));
                        productListModel.setSale_price(array.optJSONObject(i).optString("sale_price"));
                        productListModel.setDescription(array.optJSONObject(i).optString("description"));
                        productListModel.setPrice_off(array.optJSONObject(i).optString("priceoff"));
                        productListModel.setStorename(array.optJSONObject(i).optString("storename"));
                        productListModel.setSelling_type(array.optJSONObject(i).optString("selling_type"));
                        arrayList_item.add(productListModel);
                    }
                    if (arrayList_item.size() > 0) {
                        recycle_list.setVisibility(View.VISIBLE);
                        GridLayoutManager gridLayoutManager = new GridLayoutManager(ViewAllProdductActvity.this, 2);
                        recycle_list.setLayoutManager(gridLayoutManager);
                        categoryListAdpter = new CategoryListAdpter(ViewAllProdductActvity.this, arrayList_item);
                        recycle_list.setAdapter(categoryListAdpter);
                    } else {
                        recycle_list.setVisibility(View.GONE);
                        Toast.makeText(context, "No data found", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                holder.txt_shopping.setBackgroundResource(R.drawable.box_corner_blue);
                holder.txt_shopping.setTextColor(Color.parseColor("#FFFFFFFF"));
            } else {
                holder.txt_shopping.setBackgroundResource(R.drawable.box_corner_white);
                holder.txt_shopping.setTextColor(Color.parseColor("#FF000000"));
            }
            holder.txt_shopping.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    arrayList_item.clear();
                    try {
                        JSONArray array_allitmes = new JSONArray(model.getItem_array());
                        for (int i = 0; i < array_allitmes.length(); i++) {
                            ProductListModel model = new ProductListModel();
                            model.setProduct_id(array_allitmes.optJSONObject(i).optString("product_id"));
                            model.setIs_fav(array_allitmes.optJSONObject(i).optString("is_fav"));
                            model.setProduct_name(array_allitmes.optJSONObject(i).optString("product_name"));
                            model.setProduct_image(array_allitmes.optJSONObject(i).optString("product_image"));
                            model.setRegular_price(array_allitmes.optJSONObject(i).optString("regular_price"));
                            model.setSale_price(array_allitmes.optJSONObject(i).optString("sale_price"));
                            model.setDescription(array_allitmes.optJSONObject(i).optString("description"));
                            model.setPrice_off(array_allitmes.optJSONObject(i).optString("priceoff"));
                            arrayList_item.add(model);
                        }
                        if (arrayList_item.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            GridLayoutManager gridLayoutManager = new GridLayoutManager(ViewAllProdductActvity.this, 2);
                            recycle_list.setLayoutManager(gridLayoutManager);
                            categoryListAdpter = new CategoryListAdpter(ViewAllProdductActvity.this, arrayList_item);
                            recycle_list.setAdapter(categoryListAdpter);
                        } else {
                            recycle_list.setVisibility(View.GONE);
                            Toast.makeText(context, "No data found", Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    notifyDataSetChanged();
                }
            });

        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }


    }

    public class CategoryListAdpter extends RecyclerView.Adapter<CategoryListAdpter.MyViewHolder> {

        private List<ProductListModel> arrayList;
        private Context context;
        private int pos = 0;
        List<ProductListModel> local_array = null;
        private ItemFilter mFilter = new ItemFilter();

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item, img_fav;
            public TextView txt_desc, txt_item, txt_salePrice, txt_RegularPrice, txt_off;
            LinearLayout li_bg;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                img_fav = view.findViewById(R.id.img_fav);
                txt_item = view.findViewById(R.id.txt_item);
                txt_desc = view.findViewById(R.id.txt_desc);
                txt_salePrice = view.findViewById(R.id.txt_salePrice);
                txt_RegularPrice = view.findViewById(R.id.txt_RegularPrice);
                txt_off = view.findViewById(R.id.txt_off);
                li_bg = view.findViewById(R.id.li_bg);
            }
        }

        public CategoryListAdpter(Context context, List<ProductListModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
            this.local_array = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adpter_home_all_sub_category_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ProductListModel model = local_array.get(position);
            if (local_array.get(position).getProduct_image().equals("")) {

            } else {
                /*Picasso.with(context)
                        .load(local_array.get(position).getProduct_image())
                        .into(holder.img_item);
*/
                Glide
                        .with(context)
                        .load(local_array.get(position).getProduct_image())
                        .centerCrop()
                        .into(holder.img_item);

            }

            if (local_array.get(position).getProduct_name().equals("")) {

            } else {
                holder.txt_item.setText(local_array.get(position).getProduct_name());
            }

            if (local_array.get(position).getDescription().equals("")) {

            } else {
                holder.txt_desc.setText(local_array.get(position).getDescription());
            }
            if (local_array.get(position).getSale_price().equals("")) {

            } else {
                holder.txt_salePrice.setText("₹" + local_array.get(position).getSale_price());
            }
            if (local_array.get(position).getRegular_price().equals("")) {

            } else {
                holder.txt_RegularPrice.setText("₹" + local_array.get(position).getRegular_price());
            }

            if (local_array.get(position).getPrice_off().equals("")) {

            } else {
                holder.txt_off.setText("RS " + local_array.get(position).getPrice_off() + " OFF");
            }

           /*

            if (pos == position) {
                holder.li_bg.setBackgroundResource(R.drawable.bg_gray);
            } else {
                holder.li_bg.setBackgroundResource(R.drawable.bg_white);
            }
           */

            if (local_array.get(position).getIs_fav().equals("0")) {
                holder.img_fav.setBackgroundResource(R.drawable.dislike);
            } else {
                holder.img_fav.setBackgroundResource(R.drawable.like);
            }
            holder.li_bg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    Intent i = new Intent(ViewAllProdductActvity.this, Home_View_Fashion_Detail.class);
                    i.putExtra("product_id", local_array.get(position).getProduct_id());
                    i.putExtra("is_fav", local_array.get(position).getIs_fav());
                    i.putExtra("storename", local_array.get(position).getStorename());
                    i.putExtra("selling_type", local_array.get(position).getSelling_type());
                    startActivity(i);
                }
            });

            holder.img_fav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                    } else {
                        if (Utils.isNetworkAvailable(ViewAllProdductActvity.this)) {
                            if (model.getIs_fav().equals("0")) {
                                addtowishlist(local_array.get(position).getProduct_id());
                            } else {
                                removewishlist(local_array.get(position).getProduct_id());
                            }

                        } else {
                            Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }

                    }

                }

                private void addtowishlist(String product_id) {
                    String tag_string_req = "req";
                    progressDialog.setMessage("Please Wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    arrayList.clear();
                    final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtowishlist, new Response.Listener<String>() {
                        @Override
                        public void onResponse(final String response) {
                            Log.e("response", "" + response);
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                            try {
                                progressDialog.dismiss();
                                JSONObject json_main = new JSONObject(response);
                                if (json_main.optString("status").equalsIgnoreCase("1")) {
                                    offerproductlist();
                                } else {
                                    Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }

                    }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }) {
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("product_id", product_id);
                            params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                            Log.e("params", "" + Constants.addtowishlist + params);
                            return params;
                        }
                    };
                    strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtowishlist);
                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
                }

                private void removewishlist(String product_id) {
                    String tag_string_req = "req";
                    progressDialog.setMessage("Please Wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    arrayList.clear();
                    final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removewishlist, new Response.Listener<String>() {
                        @Override
                        public void onResponse(final String response) {
                            Log.e("response", "" + response);
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                            try {
                                progressDialog.dismiss();
                                JSONObject json_main = new JSONObject(response);
                                if (json_main.optString("status").equalsIgnoreCase("1")) {
                                    offerproductlist();
                                } else {
                                    Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }

                    }, new Response.ErrorListener() {
                        public void onErrorResponse(VolleyError error) {
                            if (progressDialog.isShowing())
                                progressDialog.dismiss();
                        }
                    }) {
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("product_id", product_id);
                            params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                            Log.e("params", "" + Constants.removewishlist + params);
                            return params;
                        }
                    };
                    strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    AppController.getInstance().getRequestQueue().getCache().remove(Constants.removewishlist);
                    AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
                }
            });
        }


        @Override
        public int getItemCount() {
            return local_array.size();
        }

        public void resetData() {
            local_array = arrayList;
        }

        public Filter getFilter() {
            return mFilter;
        }

        private class ItemFilter extends Filter {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                FilterResults results = new FilterResults();

                final List<ProductListModel> list = arrayList;

                int count = list.size();
                final ArrayList<ProductListModel> nlist = new ArrayList<>(count);

                if (constraint.length() == 0) {
                    nlist.addAll(arrayList);
                } else {
                    final String filterPattern = constraint.toString().toLowerCase().trim();
                    for (final ProductListModel mWords : arrayList) {
                        if ((mWords.getProduct_name().toLowerCase().contains(filterPattern))) {
                            nlist.add(mWords);
                        }
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

                return results;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                local_array = (List<ProductListModel>) results.values;
                categoryListAdpter.notifyDataSetChanged();
            }

        }

    }


    @Override
    protected void onResume() {
        if (Utils.isNetworkAvailable(ViewAllProdductActvity.this)) {
            offerproductlist();
        } else {
            Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        super.onResume();
    }
}

