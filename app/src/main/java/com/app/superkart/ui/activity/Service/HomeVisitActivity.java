package com.app.superkart.ui.activity.Service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Service.TimeSlotAdpter;
import com.app.superkart.model.Service.TimeModel;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.utils.MiddleItemFinder;
import com.app.superkart.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class HomeVisitActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private HorizontalCalanderAdapter mAdapter;
    private TimeSlotAdpter timeSlotAdpter;
    private ArrayList<TimeModel> arrayList = new ArrayList<>();

    private RecyclerView recycle_date, recycle_time;
    private String cur_date = "", CurrentdDate = "", str_Pickdate = "", str_Pickdatetime = "";
    private List<Date> dates = new ArrayList<Date>();
    private Date startDate, endDate;
    public Date Pickdate;
    DateFormat formatter;

    private RelativeLayout rl_Calender;
    private TextView txt_date, txt_confirm;
    SimpleDateFormat simpleDateFormat;
    String currentMonth = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.home_visit_service);
        Log.e("HomeVisitActivity", "HomeVisitActivity");
        init();
        simpleDateFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());
        currentMonth = simpleDateFormat.format(new Date());
        txt_date.setText(currentMonth);
        setCalander();
        setTime();
    }

    public void init() {
        recycle_date = findViewById(R.id.recycle_date);
        recycle_time = findViewById(R.id.recycle_time);
        rl_Calender = findViewById(R.id.rl_Calender);
        txt_date = findViewById(R.id.txt_date);
        txt_confirm = findViewById(R.id.txt_confirm);

        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        rl_Calender.setOnClickListener(this);
        txt_confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.txt_confirm:
                Intent intent = new Intent(mcontext, ServicePalaceOrderActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void setCalander() {
        try {

            String todayDate = getTodayDate();
            String futureDate = getAfter2MonthDate();
            String str_date = todayDate;
            String end_date = futureDate;
            formatter = new java.text.SimpleDateFormat("dd/MM/yyyy");
            startDate = (Date) formatter.parse(str_date);
            endDate = (Date) formatter.parse(end_date);
            long interval = 24 * 1000 * 60 * 60;
            long endTime = endDate.getTime();
            long curTime = startDate.getTime();
            while (curTime <= endTime) {
                dates.add(new Date(curTime));
                curTime += interval;
            }
            mAdapter = new HorizontalCalanderAdapter(HomeVisitActivity.this, dates);
            final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            recycle_date.setLayoutManager(mLayoutManager);
            recycle_date.setAdapter(mAdapter);
            recycle_date.addOnScrollListener(new MiddleItemFinder(this, mLayoutManager, callback, RecyclerView.SCROLL_STATE_IDLE));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getAfter2MonthDate() {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        calendar.add(java.util.Calendar.MONTH, +2);
        Date date = calendar.getTime();
        java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("dd/MM/yyyy");
        String futureDate = format.format(date);
        return futureDate;
    }

    private String getTodayDate() {
        java.util.Calendar c = java.util.Calendar.getInstance();
        int year = c.get(java.util.Calendar.YEAR);
        int month = c.get(java.util.Calendar.MONTH);
        int day = c.get(java.util.Calendar.DAY_OF_MONTH);

        String currentDay = String.valueOf(day);
        String currentMonth = String.valueOf(month + 1);
        String currentYear = String.valueOf(year);
        return currentDay + "/" + currentMonth + "/" + currentYear;
    }

    private void setTime() {
        arrayList.clear();
        TimeModel timeModel;
        timeModel = new TimeModel("12:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("01:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("02:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("03:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("04:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("05;00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("06:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("07:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("08:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("09:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("10:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("11:00", "AM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("12:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("01:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("02:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("03:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("04:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("05:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("06:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("07:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("08:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("09:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("10:00", "PM");
        arrayList.add(timeModel);
        timeModel = new TimeModel("11:00", "PM");
        arrayList.add(timeModel);

        timeSlotAdpter = new TimeSlotAdpter(HomeVisitActivity.this, arrayList);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recycle_time.setLayoutManager(mLayoutManager);
        recycle_time.setAdapter(timeSlotAdpter);
    }


    public class HorizontalCalanderAdapter extends RecyclerView.Adapter<HorizontalCalanderAdapter.MyViewHolder> {

        private final java.text.SimpleDateFormat formatter;
        private Context context;
        private List<Date> dataList;
        private int centerPosition = 0;
        // private onSetMonthNameListener listener;

        public HorizontalCalanderAdapter(Context context, List<Date> dates) {
            this.context = context;
            this.dataList = dates;
            // this.listener = onSetMonthNameListener;
            formatter = new java.text.SimpleDateFormat("dd/MM/yyyy");

        }

        @NonNull
        @Override
        public HorizontalCalanderAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_horizontal_calander, parent, false);
            return new HorizontalCalanderAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final HorizontalCalanderAdapter.MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {


            final Date lDate = (Date) dataList.get(position);
            final String myDate = formatter.format(lDate);
            if (!TextUtils.isEmpty(myDate)) {
                StringTokenizer tokens = new StringTokenizer(myDate, "/"); //31/8/2018
                String day = tokens.nextToken();
                holder.txt_date.setText(String.valueOf(day));
            }

            final CharSequence nameOfDay = android.text.format.DateFormat.format("EEE", lDate);
            holder.txt_day.setText(String.valueOf(nameOfDay));

            if (centerPosition == position) {
               /* CharSequence nameOfDay = android.text.format.DateFormat.format("EEEE", lDate);
                if (position == centerPosition) {
                    SelectPrefferedTimeActivity.str_Currentday = String.valueOf(nameOfDay);
                    str_Pickdate = myDate;
                    AppConstant.PickupDate = str_Pickdate;
                    try {
                        Pickdate = (Date) formatter.parse(str_Pickdate);
                        Log.e("Pickdate", "" + Pickdate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }*/
                holder.ll_parent.setBackgroundResource(R.drawable.round_bg_blue);
                holder.txt_date.setTextColor(Color.parseColor("#FFFFFFFF"));
                holder.txt_day.setTextColor(Color.parseColor("#FFFFFFFF"));
            } else {
                holder.ll_parent.setBackgroundResource(R.drawable.round_gray_border);
                holder.txt_date.setTextColor(Color.parseColor("#bdbdbd"));
                holder.txt_day.setTextColor(Color.parseColor("#bdbdbd"));
            }


            holder.ll_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    centerPosition = position;
                    notifyDataSetChanged();
                    CharSequence nameOfDay = android.text.format.DateFormat.format("EEEE", lDate);
                  /*  if (position == centerPosition) {
                        SelectPrefferedTimeActivity.str_Currentday = String.valueOf(nameOfDay);
                        Log.e("nameOfDay", "" + SelectPrefferedTimeActivity.str_Currentday);
                        Log.e("AppConstant.PickupDate", "" + AppConstant.PickupDate);
                        AppConstant.PickupDate = str_Pickdate;
                        try {
                            Pickdate = (Date) formatter.parse(str_Pickdate);
                            Log.e("on click Pickdate", "" + Pickdate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }*/
                }
            });

        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private final TextView txt_date;
            private final TextView txt_day;
            private final LinearLayout ll_parent;

            public MyViewHolder(View view) {
                super(view);
                ll_parent = (LinearLayout) view.findViewById(R.id.ll_parent);
                txt_date = view.findViewById(R.id.txt_date);
                txt_day = view.findViewById(R.id.txt_day);
            }
        }

        public void setCenterPosition(int position) {
            centerPosition = position;
            notifyDataSetChanged();
        }

        public String getMonth(int middlePosition) {
            try {
                Date lDate = (Date) dataList.get(middlePosition);
                String myDate = formatter.format(lDate);
                Date d = new java.text.SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(myDate);
                Calendar cal = Calendar.getInstance();
                cal.setTime(d);
                String monthName = new java.text.SimpleDateFormat("MMMM").format(cal.getTime());
                return monthName;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "";
        }


        @Override
        public int getItemViewType(int position) {
            return position;
        }
    }

    MiddleItemFinder.MiddleItemCallback callback = new MiddleItemFinder.MiddleItemCallback() {
        @Override
        public void scrollFinished(int middleElement) {
            mAdapter.setCenterPosition(middleElement);
            String monthName = mAdapter.getMonth(middleElement);

        }
    };
}
