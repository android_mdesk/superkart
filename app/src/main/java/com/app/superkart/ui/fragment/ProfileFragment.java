package com.app.superkart.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Shopping.AddAdressActivity;
import com.app.superkart.ui.activity.Shopping.AddressListActivity;
import com.app.superkart.ui.activity.Shopping.WhishListActivity;
import com.app.superkart.ui.activity.Signup.ChangePassword;
import com.app.superkart.ui.activity.Signup.EditProfileActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProfileFragment extends Fragment implements View.OnClickListener {
    private CircleImageView profile_image;
    private PreferenceManager preferenceManager;
    private TextView txt_userName;
    private LinearLayout li_whishlist, li_EditProfile, li_changePassword, li_my_address;
    private SharedPreferences preferences;
    private ProgressDialog progressDialog;

    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        Log.e("ProfileFragment", "ProfileFragment");
        preferences = getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        init(view);
        ClickListener();

        if (AppConstant.check_without_social.equals("0")) {

        } else {
            if ((preferences.getString(PreferenceManager.profile_pic, "").equals("") || (preferences.getString(PreferenceManager.profile_pic, "").equals("null")) ||
                    (preferences.getString(PreferenceManager.profile_pic, "").equals(null)) || (preferences.getString(PreferenceManager.profile_pic, "") == null))) {

            } else {
                Glide
                        .with(getActivity())
                        .load(preferences.getString(PreferenceManager.profile_pic, ""))
                        .centerCrop()
                        .into(profile_image);
            }

            if (preferences.getString(PreferenceManager.username, "").equals("") || (preferences.getString(PreferenceManager.username, "").equals("null")) ||
                    (preferences.getString(PreferenceManager.username, "").equals(null)) || (preferences.getString(PreferenceManager.username, "") == null)) {

            } else {
                txt_userName.setText(preferences.getString(PreferenceManager.username, ""));
            }
        }


        return view;
    }

    public void init(View view) {
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        profile_image = view.findViewById(R.id.profile_image);
        txt_userName = view.findViewById(R.id.txt_userName);
        li_whishlist = view.findViewById(R.id.li_whishlist);
        li_EditProfile = view.findViewById(R.id.li_EditProfile);
        li_changePassword = view.findViewById(R.id.li_changePassword);
        li_my_address = view.findViewById(R.id.li_my_address);
    }

    public void ClickListener() {
        li_whishlist.setOnClickListener(this);
        li_EditProfile.setOnClickListener(this);
        li_changePassword.setOnClickListener(this);
        li_my_address.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.li_whishlist:
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    // Utils.showSnackBar(v, "Please login for whislist");
                    Toast.makeText(getActivity(), "Please login for whislist.", Toast.LENGTH_LONG).show();
                } else {
                    Intent i_whishlist = new Intent(getActivity(), WhishListActivity.class);
                    startActivity(i_whishlist);
                }
                break;
            case R.id.li_EditProfile:
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    //  Utils.showSnackBar(v, "Please login for edit profile");
                    Toast.makeText(getActivity(), "Please login to edit profile.", Toast.LENGTH_LONG).show();
                } else {
                    Intent i_EditProfile = new Intent(getActivity(), EditProfileActivity.class);
                    startActivity(i_EditProfile);
                }
                break;
            case R.id.li_changePassword:
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    //  Utils.showSnackBar(v, "Please login for edit profile");
                    Toast.makeText(getActivity(), "Please login to change password.", Toast.LENGTH_LONG).show();
                } else {
                    Intent i_EditProfile = new Intent(getActivity(), ChangePassword.class);
                    startActivity(i_EditProfile);
                }
                break;
            case R.id.li_my_address:
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    //  Utils.showSnackBar(v, "Please login for edit profile");
                    Toast.makeText(getActivity(), "Please login to add address.", Toast.LENGTH_LONG).show();
                } else {
                    Intent i_address = new Intent(getActivity(), AddAdressActivity.class);
                    startActivity(i_address);
                }
                break;
        }
    }

    private void profile() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.profile, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONObject ojb_result = json_main.optJSONObject("result");
                        JSONObject obj_customer_detail = ojb_result.optJSONObject("customer_detail");
                        if (obj_customer_detail.optString("username").equals("") || obj_customer_detail.optString("username").equals("null") || obj_customer_detail.optString("username").equals(null)) {
                            txt_userName.setText(obj_customer_detail.optString("email"));
                        } else {
                            txt_userName.setText(obj_customer_detail.optString("username"));
                        }
                        if (ojb_result.has("state")) {
                            AppConstant.state = String.valueOf(ojb_result.optJSONArray("state"));
                        }

                        if (obj_customer_detail.optString("profile_pic").equals("") | obj_customer_detail.optString("profile_pic").equals("null") || obj_customer_detail.optString("profile_pic").equals(null)) {

                        } else {
                            /*Picasso.with(getActivity())
                                    .load(obj_customer_detail.optString("profile_pic"))
                                    .into(profile_image);*/

                            Glide
                                    .with(getActivity())
                                    .load(obj_customer_detail.optString("profile_pic"))
                                    .centerCrop()
                                    .into(profile_image);

                            Glide
                                    .with(getActivity())
                                    .load(obj_customer_detail.optString("profile_pic"))
                                    .centerCrop()
                                    .into(BaseActivity.profile_image);

                           /* Picasso.with(getActivity())
                                    .load(obj_customer_detail.optString("profile_pic"))
                                    .into(BaseActivity.profile_image);*/
                        }
                        SharedPreferences preferences = getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(PreferenceManager.user_id, obj_customer_detail.optString("user_id"));
                        editor.putString(PreferenceManager.username, obj_customer_detail.optString("username"));
                        editor.putString(PreferenceManager.profile_pic, obj_customer_detail.optString("profile_pic"));
                        editor.putString(PreferenceManager.phone_number, obj_customer_detail.optString("mobile"));
                        editor.putString(PreferenceManager.address_1, obj_customer_detail.optString("address_line1"));
                        editor.putString(PreferenceManager.address_2, obj_customer_detail.optString("address_line2"));
                        editor.putString(PreferenceManager.postal_code, obj_customer_detail.optString("zip"));
                        editor.putString(PreferenceManager.state, obj_customer_detail.optString("state"));
                        editor.putString(PreferenceManager.city, obj_customer_detail.optString("city"));
                        editor.commit();
                    } else {
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.profile + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.profile);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onResume() {
        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {

        } else {
            if (Utils.isNetworkAvailable(getActivity())) {
                profile();
            } else {
                Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_LONG).show();
            }
        }

        super.onResume();
    }
}

