package com.app.superkart.ui.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.model.Shopping.CartModel;
import com.app.superkart.model.Shopping.CartSubProduct;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Shopping.AddressListActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CartFragment extends Fragment implements View.OnClickListener {
    private RecyclerView recycle_item;
    private LinearLayout li_btn_placeOrder, li_error_msg;
    private RelativeLayout rel_loyalty_popup, rl_coupon_bottom;

    private LinearLayoutManager linearLayoutManager, linearLayoutManager1, linearLayoutManager_coupon;
    private FashionCartAdpter_New fashionCartAdpter_new;
    private FashionCartAdpter_Product fashionCartAdpter_product;
    private ArrayList<CartModel> arrayList_cart = new ArrayList<>();

    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private TextView txt_cart_count, txt_amount, txt_btn_continue, txt_btn_popup_loyalty, txt_popup_title;
    private ArrayList<String> array_qty = new ArrayList<>();
    private SharedPreferences pref_cart;
    private ImageView img_popclick, img_cancel;
    private EditText et_pop_loyalty;
    private ViewGroup hiddenPanel;
    private boolean isPanelShown;
    private RecyclerView recycle_coupon;
    private ArrayList<AllCouponModel> arrayList_coupon = new ArrayList<>();
    private CouponAdpter couponAdpter;
    private ImageView img_bottom_back;
    private TextView txt_btn_apply, txt_msg, txt_vendername;
    private EditText et_promocode;
    int index = 0;
    String Vendor_id = "";
    JSONObject jsonObject;

    public CartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_cart_newlist, container, false);
        Log.e("CartFragment", "CartFragment");
        init(view);
        preferences = getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);
        pref_cart = getActivity().getSharedPreferences(getActivity().getString(R.string.app_name), Context.MODE_PRIVATE);

        AppConstant.jsonArray = new JSONArray();
        for (int i = 1; i < 11; i++) {
            array_qty.add(String.valueOf(i));
        }

        return view;
    }

    public void init(View view) {
        recycle_item = view.findViewById(R.id.recycle_item);
        recycle_coupon = view.findViewById(R.id.recycle_coupon);
        txt_cart_count = view.findViewById(R.id.txt_cart_count);
        txt_amount = view.findViewById(R.id.txt_amount);
        li_error_msg = view.findViewById(R.id.li_error_msg);
        txt_btn_continue = view.findViewById(R.id.txt_btn_continue);
        li_btn_placeOrder = view.findViewById(R.id.li_btn_placeOrder);
        rel_loyalty_popup = view.findViewById(R.id.rel_loyalty_popup);
        rl_coupon_bottom = view.findViewById(R.id.rl_coupon_bottom);
        img_popclick = view.findViewById(R.id.img_popclick);
        et_pop_loyalty = view.findViewById(R.id.et_pop_loyalty);
        txt_btn_popup_loyalty = view.findViewById(R.id.txt_btn_popup_loyalty);
        txt_popup_title = view.findViewById(R.id.txt_popup_title);
        img_cancel = view.findViewById(R.id.img_cancel);
        img_bottom_back = view.findViewById(R.id.img_bottom_back);
        txt_vendername = view.findViewById(R.id.txt_vendername);
        et_promocode = view.findViewById(R.id.et_promocode);
        txt_btn_apply = view.findViewById(R.id.txt_btn_apply);
        txt_msg = view.findViewById(R.id.txt_msg);
        txt_btn_continue.setOnClickListener(this);
        img_popclick.setOnClickListener(this);
        img_cancel.setOnClickListener(this);
        txt_btn_popup_loyalty.setOnClickListener(this);
        img_bottom_back.setOnClickListener(this);
        txt_popup_title.setText("Redeem your loyalty points");
        txt_btn_popup_loyalty.setText("Redeem Now");

        hiddenPanel = (ViewGroup) view.findViewById(R.id.rl_coupon_bottom);
        hiddenPanel.setVisibility(View.INVISIBLE);
        isPanelShown = false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_btn_continue:
                AppConstant.check_cart = "card_screen";
                Intent i = new Intent(getActivity(), AddressListActivity.class);
                startActivity(i);
                break;
            case R.id.img_popclick:
                break;
            case R.id.img_cancel:
                rel_loyalty_popup.setVisibility(View.GONE);
                break;
            case R.id.txt_btn_popup_loyalty:
                String new_Lp = et_pop_loyalty.getText().toString().trim();
                cartReedemRequest(AppConstant.vender_id, AppConstant.LoyaltyPoints, new_Lp);
                break;
            case R.id.img_bottom_back:
                arrayList_coupon.clear();
                slideUpDown(v);
                break;
        }
    }

    public void cartlisttest() {
        String tag_string_req = "req";
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList_cart.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cartlisttest, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                progressDialog.dismiss();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        li_error_msg.setVisibility(View.GONE);
                        for (int i = 0; i < array_result.length(); i++) {
                            CartModel model = new CartModel();
                            model.setVendor_id(array_result.optJSONObject(i).optString("vendor_id"));
                            model.setC_name(array_result.optJSONObject(i).optString("c_name"));
                            model.setLoyaltyPoints(array_result.optJSONObject(i).optString("loyaltyPoints"));
                            model.setRequestLP(array_result.optJSONObject(i).optString("requestLP"));
                            model.setRemainingLp(array_result.optJSONObject(i).optString("remainingLp"));
                            model.setArray_product(array_result.optJSONObject(i).optString("product"));
                            model.setUserlp(array_result.optJSONObject(i).optString("userlp"));
                            model.setItem_price(array_result.optJSONObject(i).optString("productTot"));
                            model.setItem_total(array_result.optJSONObject(i).optString("productTot"));
                            model.setArray_coupon(array_result.optJSONObject(i).optString("coupon"));
                            model.setTotalWithoutDisc(array_result.optJSONObject(i).optString("totalWithoutDisc"));
                            model.setDiscountTotal(array_result.optJSONObject(i).optString("discountTotal"));
                            model.setPromocode(array_result.optJSONObject(i).optString("promocode"));
                            arrayList_cart.add(model);
                        }
                        if (arrayList_cart.size() > 0) {
                            progressDialog.dismiss();
                            li_error_msg.setVisibility(View.GONE);
                            BaseActivity.txt_cart_count.setVisibility(View.VISIBLE);
                            BaseActivity.txt_cart_count.setText(json_main.optString("totalCart"));
                            SharedPreferences pref_cart = getActivity().getSharedPreferences("cart_count", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = pref_cart.edit();
                            editor.putString(PreferenceManager.Cart_Count, json_main.optString("totalCart"));
                            editor.commit();
                            txt_amount.setVisibility(View.VISIBLE);
                            li_btn_placeOrder.setVisibility(View.VISIBLE);
                            recycle_item.setVisibility(View.VISIBLE);
                            txt_amount.setText("₹" + json_main.optString("totalamt"));
                            AppConstant.total_price = json_main.optString("totalamt");
                            linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            recycle_item.setLayoutManager(linearLayoutManager);
                            fashionCartAdpter_new = new FashionCartAdpter_New(getActivity(), arrayList_cart);
                            recycle_item.setAdapter(fashionCartAdpter_new);
                            fashionCartAdpter_new.notifyDataSetChanged();
                        } else {
                            progressDialog.dismiss();
                            //   Toast.makeText(getActivity(), "No items in cart.", Toast.LENGTH_SHORT).show();
                            SharedPreferences cart_count = getActivity().getSharedPreferences("cart_count", 0);
                            cart_count.edit().clear().commit();
                            BaseActivity.txt_cart_count.setVisibility(View.GONE);
                            txt_amount.setVisibility(View.GONE);
                            li_btn_placeOrder.setVisibility(View.GONE);
                            recycle_item.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }

                    } else {
                        progressDialog.dismiss();
                        SharedPreferences cart_count = getActivity().getSharedPreferences("cart_count", 0);
                        cart_count.edit().clear().commit();
                        BaseActivity.txt_cart_count.setVisibility(View.GONE);
                        txt_amount.setVisibility(View.GONE);
                        li_btn_placeOrder.setVisibility(View.GONE);
                        recycle_item.setVisibility(View.GONE);
                        li_error_msg.setVisibility(View.VISIBLE);
                        //Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.cartlisttest + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cartlisttest);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class FashionCartAdpter_New extends RecyclerView.Adapter<FashionCartAdpter_New.MyViewHolder> {

        private List<CartModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView txt_sellerName;
            TextView txt_price, coupon_price, txt_loyalty_price, txt_loyalty, txt_loyalty_new, txt_totalprice, txt_btn_reddemNow, txt_btn_coupon, txt_coupon_code, txt_btn_remove;
            RecyclerView recycle_productlist;
            CardView card_redeem;
            LinearLayout li_coupon;
            View view_line;
            ImageView img_1;

            public MyViewHolder(View view) {
                super(view);
                txt_price = view.findViewById(R.id.txt_price);
                coupon_price = view.findViewById(R.id.coupon_price);
                txt_loyalty_price = view.findViewById(R.id.txt_loyalty_price);
                txt_totalprice = view.findViewById(R.id.txt_totalprice);
                txt_sellerName = view.findViewById(R.id.txt_sellerName);
                txt_loyalty = view.findViewById(R.id.txt_loyalty);
                txt_loyalty_new = view.findViewById(R.id.txt_loyalty_new);
                view_line = view.findViewById(R.id.view_line);
                recycle_productlist = view.findViewById(R.id.recycle_productlist);
                txt_btn_reddemNow = view.findViewById(R.id.txt_btn_reddemNow);
                txt_btn_coupon = view.findViewById(R.id.txt_btn_coupon);
                card_redeem = view.findViewById(R.id.card_redeem);
                txt_coupon_code = view.findViewById(R.id.txt_coupon_code);
                txt_btn_remove = view.findViewById(R.id.txt_btn_remove);
                li_coupon = view.findViewById(R.id.li_coupon);
                img_1 = view.findViewById(R.id.img_1);
            }
        }

        public FashionCartAdpter_New(Context context, List<CartModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public FashionCartAdpter_New.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cart_adpter_new, parent, false);

            return new FashionCartAdpter_New.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(FashionCartAdpter_New.MyViewHolder holder, int position) {

            CartModel model = arrayList.get(position);

            if (model.getC_name().equals("") || (model.getC_name().equals("null") || (model.getC_name().equals(null)))) {

            } else {
                holder.txt_sellerName.setText(model.getC_name());
            }

            if (model.getTotalWithoutDisc().equals("") || (model.getTotalWithoutDisc().equals("null") || (model.getTotalWithoutDisc().equals(null)))) {

            } else {
                holder.txt_price.setText("₹" + model.getTotalWithoutDisc());
            }

            if (model.getItem_total().equals("") || (model.getItem_total().equals("null") || (model.getItem_total().equals(null)))) {

            } else {
                holder.txt_totalprice.setText("₹" + model.getItem_total());
            }

            if (model.getDiscountTotal().equals("") || (model.getDiscountTotal().equals("null") || (model.getDiscountTotal().equals(null)))) {

            } else {
                holder.coupon_price.setText("- ₹" + model.getDiscountTotal());
            }

            if (model.getRemainingLp().equals("0") || (model.getRemainingLp().equals("null") || (model.getRemainingLp().equals(null)))) {
                holder.txt_loyalty_new.setVisibility(View.GONE);
                holder.img_1.setVisibility(View.GONE);
            } else {
                holder.txt_loyalty_new.setVisibility(View.VISIBLE);
                holder.img_1.setVisibility(View.VISIBLE);
                holder.txt_loyalty_new.setText(model.getRemainingLp());
            }

            if (model.getLoyaltyPoints().equals("0") || (model.getLoyaltyPoints().equals("null") || (model.getLoyaltyPoints().equals(null)))) {
                holder.txt_loyalty.setText("0");
                holder.view_line.setVisibility(View.GONE);
                holder.txt_btn_reddemNow.setTextColor(Color.parseColor("#9e9e9e"));
                holder.card_redeem.setBackgroundResource(R.drawable.card_view_bg_gray);
                holder.card_redeem.setEnabled(false);
            } else {
                holder.txt_loyalty.setText(model.getLoyaltyPoints());
                holder.view_line.setVisibility(View.VISIBLE);
                holder.card_redeem.setBackgroundResource(R.drawable.card_view_bg);
                holder.card_redeem.setEnabled(true);
            }

            if (model.getRequestLP().equals("0") || (model.getRequestLP().equals("null") || (model.getRequestLP().equals(null)))) {

                holder.txt_loyalty_price.setText("0");
            } else {
                holder.txt_loyalty_price.setText(model.getRequestLP());

            }
            if (model.getPromocode().equals("") || model.getPromocode().equals("null") || model.getPromocode().equals(null)) {
                holder.li_coupon.setVisibility(View.GONE);
            } else {
                holder.li_coupon.setVisibility(View.GONE);
                holder.txt_coupon_code.setText("Promo Code: " + model.getPromocode());
            }

            try {
                ArrayList<CartSubProduct> arrayList_product = new ArrayList<>();
                JSONArray array_product = new JSONArray(model.getArray_product());
                for (int i = 0; i < array_product.length(); i++) {
                    CartSubProduct cartSubProduct = new CartSubProduct();
                    cartSubProduct.setCart_id(array_product.optJSONObject(i).optString("cart_id"));
                    cartSubProduct.setProduct_id(array_product.optJSONObject(i).optString("product_id"));
                    cartSubProduct.setProduct_name(array_product.optJSONObject(i).optString("product_name"));
                    cartSubProduct.setProduct_image(array_product.optJSONObject(i).optString("product_image"));
                    cartSubProduct.setRegular_price(array_product.optJSONObject(i).optString("regular_price"));
                    cartSubProduct.setSale_price(array_product.optJSONObject(i).optString("sale_price"));
                    cartSubProduct.setPrice_off(array_product.optJSONObject(i).optString("priceoff"));
                    cartSubProduct.setQty(array_product.optJSONObject(i).optString("qty"));
                    cartSubProduct.setDescription(array_product.optJSONObject(i).optString("description"));
                    cartSubProduct.setIs_fav(array_product.optJSONObject(i).optString("is_fav"));
                    cartSubProduct.setPromocode(array_product.optJSONObject(i).optString("promocode"));
                    cartSubProduct.setIs_coupon(array_product.optJSONObject(i).optString("is_coupon"));
                    cartSubProduct.setVendor_id(array_product.optJSONObject(i).optString("vendor_id"));
                    cartSubProduct.setCoupon_id(array_product.optJSONObject(i).optString("coupon_id"));
                    arrayList_product.add(cartSubProduct);
                }
                if (arrayList_product.size() > 0) {
                    linearLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                    holder.recycle_productlist.setLayoutManager(linearLayoutManager1);
                    fashionCartAdpter_product = new FashionCartAdpter_Product(getActivity(), arrayList_product);
                    holder.recycle_productlist.setAdapter(fashionCartAdpter_product);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            holder.card_redeem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rel_loyalty_popup.setVisibility(View.VISIBLE);
                    AppConstant.vender_id = model.getVendor_id();
                    AppConstant.LoyaltyPoints = model.getLoyaltyPoints();
                    Log.e("getVendor_id", "" + AppConstant.vender_id);
                    Log.e("getLoyaltyPoints", "" + AppConstant.LoyaltyPoints);
                }
            });

            holder.txt_btn_coupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrayList_coupon.clear();
                    slideUpDown(v);
                    Vendor_id = model.getVendor_id();
                    cartProductCoupon(model.getVendor_id(), model.getProduct_id());
                }
            });

            holder.txt_btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // removeCoupon(model.getVendor_id(), v);
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }


    }

    public class FashionCartAdpter_Product extends RecyclerView.Adapter<FashionCartAdpter_Product.MyViewHolder> {

        private List<CartSubProduct> arrayList;
        private Context context;
        String qty = "";


        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item_1;
            TextView txt_item_1, txt_amount_1, txt_remove_1, txt_move_1, txt_coupon_code, txt_btn_coupon, txt_btn_remove;
            Spinner spinner_Qty_1;
            LinearLayout li_coupon;

            public MyViewHolder(View view) {
                super(view);
                txt_item_1 = view.findViewById(R.id.txt_item_1);
                txt_amount_1 = view.findViewById(R.id.txt_amount_1);
                txt_remove_1 = view.findViewById(R.id.txt_remove_1);
                txt_move_1 = view.findViewById(R.id.txt_move_1);
                img_item_1 = view.findViewById(R.id.img_item_1);
                spinner_Qty_1 = view.findViewById(R.id.spinner_Qty_1);
                li_coupon = view.findViewById(R.id.li_coupon);
                txt_coupon_code = view.findViewById(R.id.txt_coupon_code);
                txt_btn_coupon = view.findViewById(R.id.txt_btn_coupon);
                txt_btn_remove = view.findViewById(R.id.txt_btn_remove);
            }
        }

        public FashionCartAdpter_Product(Context context, List<CartSubProduct> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public FashionCartAdpter_Product.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_adpter_new_product, parent, false);

            return new FashionCartAdpter_Product.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(FashionCartAdpter_Product.MyViewHolder holder, int position) {
            CartSubProduct model = arrayList.get(position);

            if (model.getProduct_image().equals("")) {

            } else {
               /* Picasso.with(context)
                        .load(model.getProduct_image())
                        .into(holder.img_item_1);*/

                Glide
                        .with(context)
                        .load(model.getProduct_image())
                        .centerCrop()
                        .into(holder.img_item_1);

            }

            if (model.getProduct_name().equals("")) {

            } else {
                holder.txt_item_1.setText(model.getProduct_name());
            }

            if (model.getSale_price().equals("")) {

            } else {
                holder.txt_amount_1.setText("₹" + model.getSale_price());
            }
            for (int i = 0; i < array_qty.size(); i++) {
                if (array_qty.get(i).equals(model.getQty())) {
                    index = i;
                }
            }

            ArrayAdapter<String> adptGenter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array_qty);
            adptGenter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spinner_Qty_1.setAdapter(adptGenter);
            holder.spinner_Qty_1.setSelection(index);

            holder.spinner_Qty_1.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    ((TextView) view).setTextColor(Color.BLACK);
                    qty = holder.spinner_Qty_1.getSelectedItem().toString();
                    if (qty.equals(model.getQty())) {
                    } else {
                        updatecart(model.getProduct_id(), qty);
                    }

                }

                public void onNothingSelected(AdapterView<?> parent) {
                }

            });

            if (model.getIs_coupon().equals("0")) {
                holder.txt_btn_coupon.setVisibility(View.GONE);
            } else {
                holder.txt_btn_coupon.setVisibility(View.VISIBLE);
            }

            if (model.getPromocode().equals("") || model.getPromocode().equals("null") || model.getPromocode().equals(null)) {
                holder.li_coupon.setVisibility(View.GONE);
            } else {
                holder.li_coupon.setVisibility(View.VISIBLE);
                holder.txt_coupon_code.setText("Promo Code: " + model.getPromocode());
            }

            if (model.getIs_fav().equals("1")) {
                holder.txt_move_1.setTextColor(Color.parseColor("#F89800"));
            } else {
                holder.txt_move_1.setTextColor(Color.parseColor("#9e9e9e"));
            }
            holder.txt_move_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                    } else {
                        if (model.getIs_fav().equals("0")) {
                            addtowishlist(model.getProduct_id());
                        } else {
                            removewishlist(model.getProduct_id());
                        }
                    }
                }
            });

            holder.txt_remove_1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isNetworkAvailable(getActivity())) {
                        removeItem(model.getCart_id());
                    } else {
                        Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.txt_btn_coupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    arrayList_coupon.clear();
                    slideUpDown(v);
                    Vendor_id = model.getVendor_id();
                    cartProductCoupon(model.getVendor_id(), model.getProduct_id());
                }
            });

            holder.txt_btn_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeCoupon(model.getVendor_id(), model.getCart_id(), model.getProduct_id(), v);
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        private void addtowishlist(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtowishlist, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            cartlisttest();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.productlist + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtowishlist);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void removewishlist(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removewishlist, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            cartlisttest();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.productlist + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.removewishlist);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void removeItem(String Cart_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.deletecart, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                            cartlisttest();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("cid", Cart_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.deletecart + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.deletecart);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void updatecart(String product_id, String qty) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.updatecart, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("RESULT").equalsIgnoreCase("1")) {
                            Toast.makeText(getActivity(), json_main.optString("MSG"), Toast.LENGTH_SHORT).show();
                            cartlisttest();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("qty", qty);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.updatecart + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.updatecart);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }

    private void cartProductCoupon(String v_id, String product_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList_coupon.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cartProductCoupon, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    txt_msg.setVisibility(View.GONE);
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        arrayList_coupon.clear();
                        JSONObject obj_result = json_main.optJSONObject("result");
                        txt_vendername.setText(obj_result.optString("storename"));
                        JSONArray array = obj_result.optJSONArray("coupon");
                        for (int i = 0; i < array.length(); i++) {
                            AllCouponModel allCouponModel = new AllCouponModel();
                            allCouponModel.setId(array.optJSONObject(i).optString("coupon_id"));
                            allCouponModel.setCoupon_code(array.optJSONObject(i).optString("coupon_code"));
                            allCouponModel.setNewImage_coupon(array.optJSONObject(i).optString("coupon_image"));
                            allCouponModel.setDiscount_Price(array.optJSONObject(i).optString("Discount_Price"));
                            allCouponModel.setDiscount_Type(array.optJSONObject(i).optString("Discount_Type"));
                            allCouponModel.setTo_Date(array.optJSONObject(i).optString("To_Date"));
                            allCouponModel.setFrom_Date(array.optJSONObject(i).optString("From_Date"));
                            allCouponModel.setCoupon_pro_id(array.optJSONObject(i).optString("coupon_pro_id"));
                            allCouponModel.setCategory_id(array.optJSONObject(i).optString("category_id"));
                            allCouponModel.setCoupon_Type(array.optJSONObject(i).optString("Coupon_Type"));
                            arrayList_coupon.add(allCouponModel);
                        }
                        if (arrayList_coupon.size() > 0) {
                            txt_msg.setVisibility(View.GONE);
                            recycle_coupon.setVisibility(View.VISIBLE);
                            linearLayoutManager_coupon = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                            recycle_coupon.setLayoutManager(linearLayoutManager_coupon);
                            couponAdpter = new CouponAdpter(getActivity(), arrayList_coupon);
                            recycle_coupon.setAdapter(couponAdpter);
                        } else {
                            txt_msg.setVisibility(View.VISIBLE);
                            recycle_coupon.setVisibility(View.GONE);
                        }

                    } else {
                        txt_msg.setVisibility(View.VISIBLE);
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vid", v_id);
                params.put("product_id", product_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.cartProductCoupon + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cartProductCoupon);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class CouponAdpter extends RecyclerView.Adapter<CouponAdpter.MyViewHolder> {

        private List<AllCouponModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            TextView txt_discount, txt_code, txt_apply;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_discount = view.findViewById(R.id.txt_discount);
                txt_code = view.findViewById(R.id.txt_code);
                txt_apply = view.findViewById(R.id.txt_apply);
            }
        }

        public CouponAdpter(Context context, List<AllCouponModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public CouponAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.cart_coupon_adpter, parent, false);

            return new CouponAdpter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(CouponAdpter.MyViewHolder holder, int position) {
            AllCouponModel model = arrayList.get(position);
            if (model.getNewImage_coupon().equals("") || model.getNewImage_coupon().equals("null") || model.getNewImage_coupon().equals(null)) {

            } else {
                Picasso.with(context)
                        .load(model.getNewImage_coupon())
                        .into(holder.img_item);

            }

            if (model.getDiscount_Type().equals("Percentage")) {
                if (model.getDiscount_Price().equals("") || model.getDiscount_Price().equals("null") || model.getDiscount_Price().equals(null)) {
                } else {
                    holder.txt_discount.setText("Up to " + model.getDiscount_Price() + " %" + " Off");
                }
            } else {
                if (model.getDiscount_Price().equals("") || model.getDiscount_Price().equals("null") || model.getDiscount_Price().equals(null)) {

                } else {
                    holder.txt_discount.setText("Up to ₹" + model.getDiscount_Price() + " Off");
                }
            }

            if (model.getCoupon_code().equals("") || model.getCoupon_code().equals("null") || model.getCoupon_code().equals(null)) {

            } else {
                holder.txt_code.setText(model.getCoupon_code());
            }

            holder.txt_apply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    applyCoupon(Vendor_id, model.getId(), model.getCoupon_code(), v);
                }
            });

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }


    }

    private void applyCoupon(String v_id, String c_id, String code, View v) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.applyCoupon, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        rel_loyalty_popup.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        slideUpDown(v);
                    } else {
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vid", v_id);
                params.put("coupon_id", c_id);
                params.put("couponCode", code);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.applyCoupon + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.applyCoupon);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void removeCoupon(String v_id, String c_id, String p_id, View v) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removeCoupon, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        rel_loyalty_popup.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        cartlisttest();
                    } else {
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("vid", v_id);
                params.put("cart_id", c_id);
                params.put("product_id", p_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.removeCoupon + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.removeCoupon);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void cartReedemRequest(String v_id, String previousloyalty, String loyaltyPoints) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cartReedemRequest, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        rel_loyalty_popup.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    } else {
                        rel_loyalty_popup.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("seller_id", v_id);
                params.put("previousloyalty", previousloyalty);
                params.put("cart_type", "online");
                params.put("loyaltyPoints", loyaltyPoints);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.cartReedemRequest + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cartReedemRequest);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void slideUpDown(final View view) {
        if (!isPanelShown) {
            // Show the panel
            Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_up);

            hiddenPanel.startAnimation(bottomUp);
            hiddenPanel.setVisibility(View.VISIBLE);
            isPanelShown = true;
        } else {
            // Hide the Panel
            Animation bottomDown = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_down);

            hiddenPanel.startAnimation(bottomDown);
            hiddenPanel.setVisibility(View.INVISIBLE);
            isPanelShown = false;
            cartlisttest();
        }
    }

    @Override
    public void onResume() {
        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
            Toast.makeText(getActivity(), "Please login to view your cart data.", Toast.LENGTH_SHORT).show();
        } else {
            if (Utils.isNetworkAvailable(getActivity())) {
                cartlisttest();
            } else {
                Toast.makeText(getActivity(), R.string.check_internet, Toast.LENGTH_LONG).show();
            }
        }
        super.onResume();
    }
}
