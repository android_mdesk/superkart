package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.Home_Fashion_BannerAdpter;
import com.app.superkart.model.Shopping.AllCouponModel;
import com.app.superkart.model.Shopping.ColorModel;
import com.app.superkart.model.Shopping.SizeModel;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Home_View_Fashion_Detail extends AppCompatActivity implements View.OnClickListener {
    private ViewPager mViewPager;
    public TabLayout tabLayout;
    private ArrayList<String> listimage = new ArrayList<>();
    private Home_Fashion_BannerAdpter home_fashion_bannerAdpter;
    private Context context;
    private SizeAdpter sizeAdpter;
    private ColorAdpter colorAdpter;
    private Coupon_Adpter coupon_adpter;
    private RecyclerView recycle_size, recycle_color, recycle_coupon;
    private LinearLayoutManager linearLayoutManager, linearLayoutManager1;
    private GridLayoutManager linearLayoutManager2;
    private ArrayList<SizeModel> arrayList_size = new ArrayList<>();
    private ArrayList<ColorModel> arrayList_color = new ArrayList<>();
    private ArrayList<AllCouponModel> arrayList_coupon = new ArrayList<>();
    private ImageView img_back, img_fav, img_btn_fav, img_cancel_login, img_popclick;
    private LinearLayout li_btn_addToBag, li_color, li_size, li_coupon;
    private String product_id = "", is_fav = "", qty = "", color_id = "", size_id = "";
    private ProgressDialog progressDialog;
    private TextView txt_pro_name, txt_desc, txt_specific, txt_sell_price, txt_RegularPrice, txt_priceOff, txt_fav_text, txt_shipping, txt_rate, txt_Qty;
    private String[] qnty;
    private Spinner spinner_Qty;
    private SharedPreferences preferences;
    private LinearLayout li_fav;
    private RelativeLayout rel_loyalty_popup;
    private EditText et_email, et_password;
    private TextView txt_btn_login;
    public String str_email = "", str_password = "", str_convert_image = "", storename = "", selling_type = "";
    SharedPreferences preferences_token;
    private List<String> attribute = new ArrayList<>();

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        context = this;
        Utils.showTitle(window, context);
        setContentView(R.layout.view_category_fashion_detail);
        Log.e("Home_View_Fashion_Detail", "Home_View_Fashion_Detail");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        preferences_token = getSharedPreferences("X", Context.MODE_PRIVATE);

        init();
        intentData();

        if (Utils.isNetworkAvailable(Home_View_Fashion_Detail.this)) {
            productdetail();
        } else {
            Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
    }

    public void init() {
        progressDialog = new ProgressDialog(context, R.style.AppCompatAlertDialogStyle);
        mViewPager = findViewById(R.id.mViewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        recycle_size = findViewById(R.id.recycle_size);
        img_back = findViewById(R.id.img_back);
        li_btn_addToBag = findViewById(R.id.li_btn_addToBag);
        txt_pro_name = findViewById(R.id.txt_pro_name);
        txt_specific = findViewById(R.id.txt_specific);
        txt_desc = findViewById(R.id.txt_desc);
        txt_sell_price = findViewById(R.id.txt_sell_price);
        txt_RegularPrice = findViewById(R.id.txt_RegularPrice);
        txt_priceOff = findViewById(R.id.txt_priceOff);
        img_fav = findViewById(R.id.img_fav);
        img_btn_fav = findViewById(R.id.img_btn_fav);
        txt_fav_text = findViewById(R.id.txt_fav_text);
        txt_Qty = findViewById(R.id.txt_Qty);
        rel_loyalty_popup = findViewById(R.id.rel_loyalty_popup);
        recycle_color = findViewById(R.id.recycle_color);
        li_color = findViewById(R.id.li_color);
        li_size = findViewById(R.id.li_size);
        li_coupon = findViewById(R.id.li_coupon);
        recycle_coupon = findViewById(R.id.recycle_coupon);
        //  spinner_Qty = findViewById(R.id.spinner_Qty);

        li_fav = findViewById(R.id.li_fav);
        txt_shipping = findViewById(R.id.txt_shipping);
        txt_rate = findViewById(R.id.txt_rate);

        img_popclick = findViewById(R.id.img_popclick);
        img_cancel_login = findViewById(R.id.img_cancel_login);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        txt_btn_login = findViewById(R.id.txt_btn_login);

        img_back.setOnClickListener(this);
        li_btn_addToBag.setOnClickListener(this);
        li_fav.setOnClickListener(this);
        img_cancel_login.setOnClickListener(this);
        img_popclick.setOnClickListener(this);
        txt_btn_login.setOnClickListener(this);
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            product_id = (String) bd.get("product_id");
            storename = (String) bd.get("storename");
            selling_type = (String) bd.get("selling_type");
            //    Log.e("product_id", "" + product_id);
            if (AppConstant.check_flow.equals("fromHome")) {
                img_fav.setBackgroundResource(R.drawable.dislike);
            } else {
                AppConstant.check_flow = "";
                is_fav = (String) bd.get("is_fav");
                //    Log.e("is_fav", "" + is_fav);
                if (is_fav.equals("0") || (is_fav.equals(""))) {
                    img_fav.setBackgroundResource(R.drawable.dislike);
                    img_btn_fav.setBackgroundResource(R.drawable.dislike);
                    txt_fav_text.setTextColor(Color.parseColor("#757575"));
                } else if (is_fav.equals("1")) {
                    img_fav.setBackgroundResource(R.drawable.like);
                    img_btn_fav.setBackgroundResource(R.drawable.like);
                    txt_fav_text.setTextColor(Color.parseColor("#FFA800"));
                }
            }

            if (storename.equals("")) {
                txt_specific.setVisibility(View.GONE);
            } else {
                txt_specific.setVisibility(View.VISIBLE);
                txt_specific.setText(storename);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                AppConstant.check_flow = "";
                finish();
                break;
            case R.id.li_btn_addToBag:
                AppConstant.check_cart_view = "view";
                qty = txt_Qty.getText().toString().trim();
                if (selling_type.equals("in-store")) {
                    Utils.showSnackBar(v, "This product is available for In-Store purchase only.");
                } else {
                    if (Utils.isNetworkAvailable(Home_View_Fashion_Detail.this)) {
                        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                            //Toast.makeText(context, "Please login to add product into cart.", Toast.LENGTH_SHORT).show();
                            Utils.showSnackBar(v, "Please login to add product into cart.");
                            rel_loyalty_popup.setVisibility(View.VISIBLE);
                        } else {
                            String str_attribute = TextUtils.join(",", attribute);

                            addtocart(str_attribute);
                        }

                    } else {
                        Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }

                break;
            case R.id.li_fav:
                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Toast.makeText(Home_View_Fashion_Detail.this, "Please login to make your product in whishlist", Toast.LENGTH_LONG).show();
                } else {
                    if (Utils.isNetworkAvailable(Home_View_Fashion_Detail.this)) {
                        if (is_fav.equals("0")) {
                            addtowishlist(product_id);
                            img_fav.setBackgroundResource(R.drawable.like);
                            img_btn_fav.setBackgroundResource(R.drawable.like);
                            txt_fav_text.setTextColor(Color.parseColor("#FFA800"));
                        } else {
                            removewishlist(product_id);
                            img_fav.setBackgroundResource(R.drawable.dislike);
                            img_btn_fav.setBackgroundResource(R.drawable.dislike);
                            txt_fav_text.setTextColor(Color.parseColor("#757575"));
                        }
                    } else {
                        Toast.makeText(context, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }

                break;
            case R.id.img_cancel_login:
                rel_loyalty_popup.setVisibility(View.GONE);
                break;
            case R.id.img_popclick:
                rel_loyalty_popup.setVisibility(View.GONE);
                break;
            case R.id.txt_btn_login:
                str_email = et_email.getText().toString().trim();
                str_password = et_password.getText().toString().trim();
                Utils.hideKeyboard(Home_View_Fashion_Detail.this);
                if (checkValidations1(v)) {
                    if (Utils.isNetworkAvailable(Home_View_Fashion_Detail.this)) {
                        Login(v, str_email, str_password);
                    } else {
                        Utils.showSnackBar(v, getResources().getString(R.string.check_internet));
                    }
                }
                break;
        }
    }

    private void productdetail() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        listimage.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.productdetail, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONObject obj_result = json_main.optJSONObject("result");
                        JSONArray array_product_detail = obj_result.optJSONArray("product_detail");
                        for (int i = 0; i < array_product_detail.length(); i++) {
                            if (array_product_detail.optJSONObject(i).optString("product_name").equals("")) {
                                txt_pro_name.setText("");
                            } else {
                                txt_pro_name.setText(array_product_detail.optJSONObject(i).optString("product_name"));
                            }
                            if (array_product_detail.optJSONObject(i).optString("description").equals("")) {
                                txt_desc.setVisibility(View.GONE);
                            } else {
                                txt_desc.setVisibility(View.VISIBLE);
                                txt_desc.setText(array_product_detail.optJSONObject(i).optString("description"));
                            }
                            if (array_product_detail.optJSONObject(i).optString("sale_price").equals("")) {
                                txt_sell_price.setText("");
                            } else {
                                txt_sell_price.setText("₹" + array_product_detail.optJSONObject(i).optString("sale_price"));
                            }
                            if (array_product_detail.optJSONObject(i).optString("regular_price").equals("")) {
                                txt_RegularPrice.setText("");
                            } else {
                                txt_RegularPrice.setText("₹" + array_product_detail.optJSONObject(i).optString("regular_price"));
                            }
                            if (array_product_detail.optJSONObject(i).optString("priceoff").equals("")) {
                                txt_priceOff.setText("");
                            } else {
                                txt_priceOff.setText("RS " + array_product_detail.optJSONObject(i).optString("priceoff") + " OFF");
                            }
                            if (array_product_detail.optJSONObject(i).optString("freeshipping").equals("1")) {
                                txt_shipping.setVisibility(View.VISIBLE);
                            } else {
                                txt_shipping.setVisibility(View.GONE);
                            }

                            if (array_product_detail.optJSONObject(i).optString("rating").equals("null") || array_product_detail.optJSONObject(i).optString("rating").equals(null) || array_product_detail.optJSONObject(i).optString("rating").equals(null)) {
                                txt_rate.setVisibility(View.GONE);
                            } else {
                                txt_rate.setText(array_product_detail.optJSONObject(i).optString("rating") + ".0");
                            }

                            JSONArray product_sub_image = array_product_detail.optJSONObject(i).optJSONArray("product_sub_image");
                            for (int j = 0; j < product_sub_image.length(); j++) {
                                listimage.add(product_sub_image.optString(j));
                            }
                            if (listimage.size() > 0) {
                                mViewPager.setVisibility(View.VISIBLE);
                                tabLayout.setVisibility(View.VISIBLE);
                                home_fashion_bannerAdpter = new Home_Fashion_BannerAdpter(Home_View_Fashion_Detail.this, listimage);
                                mViewPager.setAdapter(home_fashion_bannerAdpter);
                                tabLayout.setupWithViewPager(mViewPager, true);
                            } else {
                                mViewPager.setVisibility(View.GONE);
                                tabLayout.setVisibility(View.GONE);
                            }

                            if (array_product_detail.optJSONObject(i).optString("hase_product_attr").equals("0")) {
                                recycle_size.setVisibility(View.GONE);
                            } else {
                                JSONObject obj_product_attributes = array_product_detail.optJSONObject(i).optJSONObject("product_attributes");
                                JSONArray array_Size = obj_product_attributes.optJSONArray("Size");
                                JSONArray array_color = obj_product_attributes.optJSONArray("Color");
                                BindSize(array_Size);
                                BindColor(array_color);
                            }
                        }

                        JSONArray array_productCoupon = obj_result.optJSONArray("productCoupon");
                        coupon(array_productCoupon);
                    } else {
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", product_id);
                Log.e("params", "" + Constants.productdetail + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.productdetail);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void addtocart(String str_attribute) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        listimage.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtocart, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("RESULT").equalsIgnoreCase("1")) {
                        Toast.makeText(context, json_main.optString("MSG"), Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(Home_View_Fashion_Detail.this, Add_To_Bag_Activity.class);
                        startActivity(i);
                    } else {
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", product_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("qty", qty);
                params.put("oftype", "instore");
                params.put("attribute_id", str_attribute);
                Log.e("params", "" + Constants.addtocart + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtocart);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void addtowishlist(String product_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtowishlist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                    } else {
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", product_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.productlist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtowishlist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void removewishlist(String product_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removewishlist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                    } else {
                        Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("product_id", product_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.productlist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.removewishlist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void Login(View v, String email, String password) {
        ProgressDialog progressDialog_main = new ProgressDialog(Home_View_Fashion_Detail.this, R.style.AppCompatAlertDialogStyle);
        progressDialog_main.setMessage("Loading...");
        progressDialog_main.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog_main.setIndeterminate(true);
        progressDialog_main.setCancelable(false);
        progressDialog_main.show();
        String url = Constants.Login;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            progressDialog_main.dismiss();
                            Utils.hideKeyboard(Home_View_Fashion_Detail.this);
                            JSONObject obj_main = new JSONObject(response);
                            Log.e("login response", response);
                            if (obj_main.optString("status").equals("1")) {
                                JSONObject obj_result = obj_main.getJSONObject("result");
                                SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(PreferenceManager.user_id, obj_result.optString("user_id"));
                                editor.putString(PreferenceManager.email_id, obj_result.optString("email"));
                                editor.putString(PreferenceManager.username, obj_result.optString("username"));
                                editor.putString(PreferenceManager.phone_number, obj_result.optString("mobile"));
                                editor.putString(PreferenceManager.address_1, obj_result.optString("address_line1"));
                                editor.putString(PreferenceManager.address_2, obj_result.optString("address_line2"));
                                editor.putString(PreferenceManager.postal_code, obj_result.optString("zip"));
                                editor.putString(PreferenceManager.state, obj_result.optString("state"));
                                editor.putString(PreferenceManager.city, obj_result.optString("city"));
                                editor.putString(PreferenceManager.user_type, obj_result.optString("user_type"));
                                editor.putString(PreferenceManager.token_in_return, obj_result.optString("token"));
                                editor.putString(PreferenceManager.profile_pic, obj_result.optString("profile_pic"));
                                editor.commit();
                                Toast.makeText(Home_View_Fashion_Detail.this, "Now you can add product in cart.", Toast.LENGTH_LONG).show();
                                rel_loyalty_popup.setVisibility(View.GONE);
                                et_email.setText("");
                                et_password.setText("");
                            } else {
                                Utils.showSnackBar(v, obj_main.optString("msg"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if (progressDialog_main.isShowing())
                                progressDialog_main.dismiss();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog_main.isShowing())
                            progressDialog_main.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after some time!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        // Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                        Utils.showSnackBar(v, message);
                        //   showSnack(message);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                //params.put("Content-Type", "multipart/form-data");
                //  params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                params.put("device_id", preferences_token.getString(PreferenceManager.DeivceToken, ""));
                params.put("regi_from", Constants.regi_from);
                //params.put("token", preferences.getString(PreferenceManager.token_in_return, ""));
                Log.e("params", "" + url + params);
                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void coupon(JSONArray jsonArray) {
        arrayList_coupon.clear();
        for (int i = 0; i < jsonArray.length(); i++) {
            AllCouponModel model = new AllCouponModel();
            model.setId(jsonArray.optJSONObject(i).optString("Coupon_Id"));
            model.setCoupon_code(jsonArray.optJSONObject(i).optString("Coupon_Code"));
            model.setNewImage_coupon(jsonArray.optJSONObject(i).optString("image"));
            model.setDiscount_Price(jsonArray.optJSONObject(i).optString("Discount_Price"));
            model.setDiscount_Type(jsonArray.optJSONObject(i).optString("Discount_Type"));
            arrayList_coupon.add(model);
        }
        if (arrayList_coupon.size() > 0) {
            recycle_coupon.setVisibility(View.VISIBLE);
            li_coupon.setVisibility(View.VISIBLE);
            linearLayoutManager2 = new GridLayoutManager(Home_View_Fashion_Detail.this, 2);
            recycle_coupon.setLayoutManager(linearLayoutManager2);
            coupon_adpter = new Coupon_Adpter(Home_View_Fashion_Detail.this, arrayList_coupon);
            recycle_coupon.setAdapter(coupon_adpter);
        } else {
            recycle_coupon.setVisibility(View.GONE);
            li_coupon.setVisibility(View.GONE);
        }

    }

    public class Coupon_Adpter extends RecyclerView.Adapter<Coupon_Adpter.MyViewHolder> {

        private List<AllCouponModel> arrayList;
        private Context context;


        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            public TextView txt_item;
            LinearLayout li_bg;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_item = view.findViewById(R.id.txt_item);
                li_bg = view.findViewById(R.id.li_bg);

            }
        }

        public Coupon_Adpter(Context context, List<AllCouponModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.home_coupon_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            AllCouponModel model = arrayList.get(position);

          /*  Picasso.with(context)
                    .load(model.getNewImage_coupon())
                    .into(holder.img_item);*/

            if (model.getNewImage_coupon().equals("") || model.getNewImage_coupon().equals("null") || model.getNewImage_coupon().equals(null)) {
            } else {
                Glide
                        .with(context)
                        .load(model.getNewImage_coupon())
                        .centerCrop()
                        .into(holder.img_item);
            }
            holder.txt_item.setText(model.getCoupon_code());
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    public void BindSize(JSONArray array_Size) {
        arrayList_size.clear();
        for (int i = 0; i < array_Size.length(); i++) {
            SizeModel model = new SizeModel();
            model.setId(array_Size.optJSONObject(i).optString("id"));
            model.setProduct_id(array_Size.optJSONObject(i).optString("product_id"));
            model.setAttr_name(array_Size.optJSONObject(i).optString("attr_name"));
            model.setAttr_val(array_Size.optJSONObject(i).optString("attr_val"));
            model.setAttr_lable(array_Size.optJSONObject(i).optString("attr_lable"));
            model.setAttr_field(array_Size.optJSONObject(i).optString("attr_field"));
            arrayList_size.add(model);
        }
        if (arrayList_size.size() > 0) {
            li_size.setVisibility(View.VISIBLE);
            recycle_size.setVisibility(View.VISIBLE);
            linearLayoutManager = new LinearLayoutManager(Home_View_Fashion_Detail.this, LinearLayoutManager.HORIZONTAL, false);
            recycle_size.setLayoutManager(linearLayoutManager);
            sizeAdpter = new SizeAdpter(Home_View_Fashion_Detail.this, arrayList_size);
            recycle_size.setAdapter(sizeAdpter);
        } else {
            li_size.setVisibility(View.GONE);
            recycle_size.setVisibility(View.GONE);
        }

    }

    public class SizeAdpter extends RecyclerView.Adapter<SizeAdpter.MyViewHolder> {

        private List<SizeModel> arrayList;
        private Context context;
        private int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txt_size;


            public MyViewHolder(View view) {
                super(view);
                txt_size = view.findViewById(R.id.txt_size);

            }
        }

        public SizeAdpter(Context context, List<SizeModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.size_adpter, parent, false);


            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            SizeModel model = arrayList.get(position);
            holder.txt_size.setText(model.getAttr_val());

            if (pos == position) {
                // holder.txt_size.setBackgroundResource(R.drawable.box_sqaure_yellow);
                holder.txt_size.setTextColor(Color.parseColor("#FFA800"));
            } else {
                //  holder.txt_size.setBackgroundResource(R.drawable.box_trasperent);
                holder.txt_size.setTextColor(Color.parseColor("#FF000000"));
            }

            holder.txt_size.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    size_id = model.getId();
                    attribute.add(size_id);
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }


    public void BindColor(JSONArray array_color) {
        arrayList_color.clear();
        for (int i = 0; i < array_color.length(); i++) {
            ColorModel model = new ColorModel();
            model.setId(array_color.optJSONObject(i).optString("id"));
            model.setProduct_id(array_color.optJSONObject(i).optString("product_id"));
            model.setAttr_name(array_color.optJSONObject(i).optString("attr_name"));
            model.setAttr_val(array_color.optJSONObject(i).optString("attr_val"));
            model.setAttr_lable(array_color.optJSONObject(i).optString("attr_lable"));
            model.setAttr_field(array_color.optJSONObject(i).optString("attr_field"));
            arrayList_color.add(model);
        }
        if (arrayList_color.size() > 0) {
            li_color.setVisibility(View.VISIBLE);
            recycle_color.setVisibility(View.VISIBLE);
            linearLayoutManager1 = new LinearLayoutManager(Home_View_Fashion_Detail.this, LinearLayoutManager.HORIZONTAL, false);
            recycle_color.setLayoutManager(linearLayoutManager1);
            colorAdpter = new ColorAdpter(Home_View_Fashion_Detail.this, arrayList_color);
            recycle_color.setAdapter(colorAdpter);
        } else {
            recycle_color.setVisibility(View.GONE);
            li_color.setVisibility(View.GONE);
        }
    }

    public class ColorAdpter extends RecyclerView.Adapter<ColorAdpter.MyViewHolder> {

        private List<ColorModel> arrayList;
        private Context context;
        private int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txt_size;
            LinearLayout li_main;

            public MyViewHolder(View view) {
                super(view);
                txt_size = view.findViewById(R.id.txt_size);
                li_main = view.findViewById(R.id.li_main);
            }
        }

        public ColorAdpter(Context context, List<ColorModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.color_adpter, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ColorModel model = arrayList.get(position);
            holder.txt_size.setBackgroundColor(Color.parseColor(model.getAttr_val()));

            if (pos == position) {
                holder.li_main.setBackgroundResource(R.drawable.box_sqaure_yellow);
                // holder.txt_size.setTextColor(Color.parseColor("#FFA800"));
            } else {
                holder.li_main.setBackgroundResource(R.drawable.box_trasperent);
                // holder.txt_size.setTextColor(Color.parseColor("#FF000000"));
            }

            holder.txt_size.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pos = position;
                    color_id = model.getId();
                    attribute.add(color_id);
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_email.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.pleaseenteremail));
            result = false;
        } else if (!Utils.emailValidator(et_email.getText().toString())) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertvalidemail));
            result = false;
        } else if (et_password.getText().toString().equals("")) {
            Utils.showSnackBar(v, getResources().getString(R.string.alertpassword));
            result = false;
        }
        return result;
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        AppConstant.check_flow = "";
        super.onBackPressed();
    }
}
