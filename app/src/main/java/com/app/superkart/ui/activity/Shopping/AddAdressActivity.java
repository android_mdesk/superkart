package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.Signup.EditProfileActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AddAdressActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mcontext;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private ArrayList<HomeCategoryModel> arrayList = new ArrayList<HomeCategoryModel>();
    private ArrayList<String> array_str_state = new ArrayList<>();
    private Spinner spinner_state;
    private String state = "", address_type = "";
    private ImageView img_back, img_home, img_office;
    private TextView btn_Add, txt_title;
    private EditText et_name, et_phone, et_address_1, et_address_2, et_city, et_pincode;
    int index = 0;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.add_address_layout);
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        pref = getSharedPreferences("ShipingAddress", MODE_PRIVATE);
        Log.e("AddAdressActivity", "AddAdressActivity");
        init();
        address_type = "Home";
        img_home.setBackgroundResource(R.drawable.tick_button);
        img_office.setBackgroundResource(R.drawable.cheched);
        if (Utils.isNetworkAvailable(AddAdressActivity.this)) {
            State();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        if (AppConstant.chk_edt.equals("edit")) {
            btn_Add.setText("Update");
            txt_title.setText("Update Address");
            if (AppConstant.Addressline1.equals("")) {
                et_address_1.setText("");
            } else {
                et_address_1.setText(AppConstant.Addressline1);
            }
            if (AppConstant.Addressline2.equals("")) {
                et_address_2.setText("");
            } else {
                et_address_2.setText(AppConstant.Addressline2);
            }
            if (AppConstant.city.equals("")) {
                et_city.setText("");
            } else {
                et_city.setText(AppConstant.city);
            }
            if (AppConstant.zip.equals("")) {
                et_pincode.setText("");
            } else {
                et_pincode.setText(AppConstant.zip);
            }
        } else {
            btn_Add.setText("Add");
            txt_title.setText("Add Address");
            et_address_1.setText("");
            et_address_2.setText("");
            et_city.setText("");
            et_pincode.setText("");
        }


        spinner_state.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ((TextView) view).setTextColor(Color.BLACK);
                Utils.hideKeyboard(AddAdressActivity.this);
                state = spinner_state.getSelectedItem().toString();
                Log.e("state", "" + state);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        spinner_state = findViewById(R.id.spinner_state);
        img_back = findViewById(R.id.img_back);
        img_home = findViewById(R.id.img_home);
        img_office = findViewById(R.id.img_office);
        btn_Add = findViewById(R.id.btn_Add);
        txt_title = findViewById(R.id.txt_title);
        et_address_1 = findViewById(R.id.et_address_1);
        et_address_2 = findViewById(R.id.et_address_2);
        et_city = findViewById(R.id.et_city);
        et_pincode = findViewById(R.id.et_pincode);
        img_back.setOnClickListener(this);
        btn_Add.setOnClickListener(this);
        img_home.setOnClickListener(this);
        img_office.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                AppConstant.state_address = "";
                AppConstant.chk_edt = "";
                AppConstant.address_id = "";
                finish();
                break;
            case R.id.btn_Add:
                if (checkValidations1(v)) {
                    if (Utils.isNetworkAvailable(AddAdressActivity.this)) {
                        String add_1 = et_address_1.getText().toString().trim();
                        String add_2 = et_address_2.getText().toString().trim();
                        String city = et_city.getText().toString().trim();
                        String pincode = et_pincode.getText().toString().trim();
                        Utils.hideKeyboard(AddAdressActivity.this);
                        if (AppConstant.chk_edt.equals("edit")) {
                            updateaddress(add_1, add_2, city, state, pincode);
                        } else {
                            addaddress(add_1, add_2, city, state, pincode);
                        }

                    } else {
                        Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }

                break;
            case R.id.img_home:
                address_type = "Home";
                img_home.setBackgroundResource(R.drawable.tick_button);
                img_office.setBackgroundResource(R.drawable.cheched);
                break;
            case R.id.img_office:
                address_type = "Office";
                img_home.setBackgroundResource(R.drawable.cheched);
                img_office.setBackgroundResource(R.drawable.tick_button);
                break;
        }
    }

    private void State() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    arrayList.clear();
                    array_str_state.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray array = json_main.optJSONArray("state");
                        for (int i = 0; i < array.length(); i++) {
                            HomeCategoryModel model = new HomeCategoryModel();
                            model.setState_id(array.optJSONObject(i).optString("id"));
                            model.setState(array.optJSONObject(i).optString("name"));
                            arrayList.add(model);
                        }
                        array_str_state.add("State");
                        if (arrayList.size() > 0) {
                            for (int j = 0; j < arrayList.size(); j++) {
                                array_str_state.add(arrayList.get(j).getState());
                            }
                        }
                        if (AppConstant.state_address.equals("")) {
                            ArrayAdapter<String> adptGenter = new ArrayAdapter<String>(mcontext, android.R.layout.simple_spinner_dropdown_item, array_str_state);
                            adptGenter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_state.setAdapter(adptGenter);
                        } else {
                            for (int i = 0; i < array_str_state.size(); i++) {
                                if (AppConstant.state_address.equals("") || AppConstant.state_address.equals("null") || AppConstant.state_address.equals(null)) {
                                    index = 0;
                                } else {
                                    if (array_str_state.get(i).equals(AppConstant.state_address)) {
                                        index = i;
                                    }
                                }
                            }

                            ArrayAdapter<String> adptGenter = new ArrayAdapter<String>(mcontext, android.R.layout.simple_spinner_dropdown_item, array_str_state);
                            adptGenter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinner_state.setAdapter(adptGenter);
                            spinner_state.setSelection(index);
                        }

                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.addaddress + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.addaddress);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void addaddress(String add_1, String add_2, String city, String state, String pincode) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    arrayList.clear();
                    array_str_state.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("address_type", address_type);
                params.put("city", city);
                params.put("state", state);
                params.put("address_line1", add_1);
                params.put("address_line2", add_2);
                params.put("zip", pincode);
                Log.e("params", "" + Constants.addaddress + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.addaddress);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void updateaddress(String add_1, String add_2, String city, String state, String pincode) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.updateaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    arrayList.clear();
                    array_str_state.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                if (AppConstant.address_id.equals("")) {
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    params.put("address_type", "Office");
                    params.put("city", city);
                    params.put("state", state);
                    params.put("address_line1", add_1);
                    params.put("address_line2", add_2);
                    params.put("zip", pincode);
                } else {
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    params.put("address_type", "Office");
                    params.put("address_id", pref.getString("ShipingId", ""));
                    params.put("city", city);
                    params.put("state", state);
                    params.put("address_line1", add_1);
                    params.put("address_line2", add_2);
                    params.put("zip", pincode);
                }

                Log.e("params", "" + Constants.updateaddress + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.updateaddress);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private boolean checkValidations1(View v) {
        boolean result = true;
        if (et_address_1.getText().toString().equals("")) {
            Utils.showSnackBar(v, "Please add your address line 1.");
            result = false;
        } else if (et_address_1.getText().toString().equals("")) {
            Utils.showSnackBar(v, "Please add your address line 2.");
            result = false;
        } else if (et_city.getText().toString().equals("")) {
            Utils.showSnackBar(v, "Please add your city.");
            result = false;
        } else if (state.equals("State")) {
            Utils.showSnackBar(v, "Please add your state.");
            result = false;
        } else if (et_pincode.getText().toString().equals("")) {
            Utils.showSnackBar(v, "Please add your zip code.");
            result = false;
        }
        return result;
    }

}
