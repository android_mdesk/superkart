package com.app.superkart.ui.activity.Signup;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.Shopping.GlobalSearchActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utility;
import com.app.superkart.utils.Utils;
import com.app.superkart.utils.VolleyMultipartRequest;
import com.app.superkart.utils.VolleySingleton;
import com.app.superkart.utils.WebAPIRequest;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private Context mcontext;
    private ImageView img_back, img_upload, profile_image;
    private EditText et_name, et_phone, et_address_1, et_address_2, et_city, et_pincode;
    private TextView et_email, btn_update, txt_userName;
    private Spinner spinner_state;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private String state = "";
    private ArrayList<String> array_str_state = new ArrayList<>();
    private ArrayList<HomeCategoryModel> arrayList = new ArrayList<HomeCategoryModel>();
    Bitmap mBitmap;
    String selectedPath = "", mPath = "";
    private static final int SELECT_IMAGE = 4;
    private int MY_REQUEST_CODE, REQUEST_CODE;
    File photo;
    private ArrayList<Bitmap> mTempBitmapArray = new ArrayList<Bitmap>();
    ArrayList<Bitmap> image_array = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.edit_profile_layout);
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        Log.e("EditProfileActivity", "EditProfileActivity");
        init();
        ClickListener();

        if (preferences.getString(PreferenceManager.profile_pic, "").equals("") || preferences.getString(PreferenceManager.profile_pic, "").equals("null") || preferences.getString(PreferenceManager.profile_pic, "").equals(null)) {
        } else {
           /* Picasso.with(EditProfileActivity.this)
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .into(profile_image);*/

            Glide
                    .with(EditProfileActivity.this)
                    .load(preferences.getString(PreferenceManager.profile_pic, ""))
                    .centerCrop()
                    .into(profile_image);

        }

        if (preferences.getString(PreferenceManager.username, "").equals("") || preferences.getString(PreferenceManager.username, "").equals("null") || preferences.getString(PreferenceManager.username, "").equals(null)) {
            et_name.setText("");
        } else {
            et_name.setText(preferences.getString(PreferenceManager.username, ""));
            txt_userName.setText(preferences.getString(PreferenceManager.username, ""));
        }

        if (preferences.getString(PreferenceManager.email_id, "").equals("") || preferences.getString(PreferenceManager.email_id, "").equals("null") || preferences.getString(PreferenceManager.email_id, "").equals(null)) {
            et_email.setText("");
        } else {
            et_email.setText(preferences.getString(PreferenceManager.email_id, ""));
        }

        if (preferences.getString(PreferenceManager.phone_number, "").equals("") || preferences.getString(PreferenceManager.phone_number, "").equals("null") || preferences.getString(PreferenceManager.phone_number, "").equals(null)) {
            et_phone.setText("");
        } else {
            et_phone.setText(preferences.getString(PreferenceManager.phone_number, ""));
        }

        if (preferences.getString(PreferenceManager.address_1, "").equals("") || preferences.getString(PreferenceManager.address_1, "").equals("null") || preferences.getString(PreferenceManager.address_1, "").equals(null)) {
            et_address_1.setText("");
        } else {
            et_address_1.setText(preferences.getString(PreferenceManager.address_1, ""));
        }

        if (preferences.getString(PreferenceManager.address_2, "").equals("") || preferences.getString(PreferenceManager.address_2, "").equals("null") || preferences.getString(PreferenceManager.address_2, "").equals(null)) {
            et_address_2.setText("");
        } else {
            et_address_2.setText(preferences.getString(PreferenceManager.address_2, ""));
        }

        if (preferences.getString(PreferenceManager.city, "").equals("") || preferences.getString(PreferenceManager.city, "").equals("null") || preferences.getString(PreferenceManager.city, "").equals(null)) {
            et_city.setText("");
        } else {
            et_city.setText(preferences.getString(PreferenceManager.city, ""));
        }

        if (preferences.getString(PreferenceManager.postal_code, "").equals("") || preferences.getString(PreferenceManager.postal_code, "").equals("null") || preferences.getString(PreferenceManager.postal_code, "").equals(null)) {
            et_pincode.setText("");
        } else {
            et_pincode.setText(preferences.getString(PreferenceManager.postal_code, ""));
        }
        Log.e("State", "" + AppConstant.state);
        try {
            JSONArray array = new JSONArray(AppConstant.state);
            arrayList.clear();
            for (int i = 0; i < array.length(); i++) {
                HomeCategoryModel model = new HomeCategoryModel();
                model.setState_id(array.optJSONObject(i).optString("id"));
                model.setState(array.optJSONObject(i).optString("name"));
                arrayList.add(model);
            }
            int index = 0;
            array_str_state.clear();
            if (arrayList.size() > 0) {
                for (int j = 0; j < arrayList.size(); j++) {
                    array_str_state.add(arrayList.get(j).getState());
                }
                for (int i = 0; i < array_str_state.size(); i++) {
                    if (preferences.getString(PreferenceManager.state, "").equals("") || preferences.getString(PreferenceManager.state, "").equals("null") || preferences.getString(PreferenceManager.state, "").equals(null)) {
                        index = 0;
                    } else {
                        if (array_str_state.get(i).equals(preferences.getString(PreferenceManager.state, ""))) {
                            index = i;
                        }
                    }
                }

            }
            ArrayAdapter<String> adptGenter = new ArrayAdapter<String>(mcontext, android.R.layout.simple_spinner_dropdown_item, array_str_state);
            adptGenter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_state.setAdapter(adptGenter);
            spinner_state.setSelection(index);
            spinner_state.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    ((TextView) view).setTextColor(Color.BLACK);
                    Utils.hideKeyboard(EditProfileActivity.this);
                    state = spinner_state.getSelectedItem().toString();
                    Log.e("state", "" + state);
                }

                public void onNothingSelected(AdapterView<?> parent) {
                }

            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        img_back = findViewById(R.id.img_back);
        img_upload = findViewById(R.id.img_upload);
        profile_image = findViewById(R.id.profile_image);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_phone = findViewById(R.id.et_phone);
        et_address_1 = findViewById(R.id.et_address_1);
        et_address_2 = findViewById(R.id.et_address_2);
        et_city = findViewById(R.id.et_city);
        et_pincode = findViewById(R.id.et_pincode);
        btn_update = findViewById(R.id.btn_update);
        spinner_state = findViewById(R.id.spinner_state);
        txt_userName = findViewById(R.id.txt_userName);
    }

    public void ClickListener() {
        img_back.setOnClickListener(this);
        btn_update.setOnClickListener(this);
        img_upload.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.img_upload:
                selectimage();
                break;
            case R.id.btn_update:
                if (Utils.isNetworkAvailable(EditProfileActivity.this)) {
                    String name = et_name.getText().toString();
                    String email = et_email.getText().toString().trim();
                    String phone = et_phone.getText().toString().trim();
                    String add_1 = et_address_1.getText().toString().trim();
                    String add_2 = et_address_2.getText().toString().trim();
                    String city = et_city.getText().toString().trim();
                    String pincode = et_pincode.getText().toString().trim();
                    Utils.hideKeyboard(EditProfileActivity.this);
                    updateprofile(name, email, phone, add_1, add_2, city, pincode);
                } else {
                    Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private void updateprofile(String name, String email, String phone, String add_1, String add_2, String city, String pincode) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.updateprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        JSONObject ojb_result = json_main.optJSONObject("result");
                        JSONObject obj_customer_detail = ojb_result.optJSONObject("customer_detail");
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();

                        if (obj_customer_detail.optString("username").equals("") || obj_customer_detail.optString("username").equals("null") || obj_customer_detail.optString("username").equals(null)) {
                            et_name.setText("");
                            txt_userName.setText("");
                        } else {
                            et_name.setText(obj_customer_detail.optString("username"));
                            txt_userName.setText(obj_customer_detail.optString("username"));
                        }
                        if (obj_customer_detail.optString("mobile").equals("") || obj_customer_detail.optString("mobile").equals("null") || obj_customer_detail.optString("mobile").equals(null)) {
                            et_phone.setText("");
                        } else {
                            et_phone.setText(obj_customer_detail.optString("mobile"));
                        }
                        if (obj_customer_detail.optString("city").equals("") || obj_customer_detail.optString("city").equals("null") || obj_customer_detail.optString("city").equals(null)) {
                            et_city.setText("");
                        } else {
                            et_city.setText(obj_customer_detail.optString("city"));
                        }
                        if (obj_customer_detail.optString("address_line1").equals("") || obj_customer_detail.optString("address_line1").equals("null") || obj_customer_detail.optString("address_line1").equals(null)) {
                            et_address_1.setText("");
                        } else {
                            et_address_1.setText(obj_customer_detail.optString("address_line1"));
                        }
                        if (obj_customer_detail.optString("address_line2").equals("") || obj_customer_detail.optString("address_line2").equals("null") || obj_customer_detail.optString("address_line2").equals(null)) {
                            et_address_2.setText("");
                        } else {
                            et_address_2.setText(obj_customer_detail.optString("address_line2"));
                        }
                        if (obj_customer_detail.optString("zip").equals("") || obj_customer_detail.optString("zip").equals("null") || obj_customer_detail.optString("zip").equals(null)) {
                            et_pincode.setText("");
                        } else {
                            et_pincode.setText(obj_customer_detail.optString("zip"));
                        }

                        finish();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("name", name);
                params.put("city", city);
                params.put("state", state);
                params.put("mobile", phone);
                params.put("address_line1", add_1);
                params.put("address_line2", add_2);
                params.put("zip", pincode);
                Log.e("params", "" + Constants.updateprofile + params);
                return params;
            }


        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.updateprofile);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private class update_profile extends AsyncTask<String, String, String> {
        private String imageupload = Constants.profilePic;
        private String response;
        String IMAGE_NAME, success, success_msg;
        JSONObject jsonObject, jsonObject_result, jsonObject_detail;
        String erro_msg, status;
        private byte[] image_bytes = null;
        private WebAPIRequest webapirequest = new WebAPIRequest();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.e("onPreExecute", "onPreExecute");
            progressDialog = new ProgressDialog(EditProfileActivity.this, R.style.AppCompatAlertDialogStyle);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... args) {
            Log.e("doInBackground", "doInBackground");
            image_bytes = getBitmapAsByteArray(mBitmap);
            mTempBitmapArray.add(mBitmap);
            image_bytes = getBitmapAsByteArray(mTempBitmapArray.get(0));
            Random r = new Random();
            int i1 = r.nextInt(80 - 65) + 65;
            String res = i1 + ".png";
            Log.e("res", "" + res);
            Log.e("params", webapirequest.update_profile(image_bytes, imageupload, res, preferences.getString(PreferenceManager.user_id, "")));
            response = webapirequest.update_profile(image_bytes, imageupload, res, preferences.getString(PreferenceManager.user_id, ""));
            Log.e("response", Constants.profilePic + response);
            try {
                jsonObject = new JSONObject(response);
                status = jsonObject.getString("1");
                jsonObject_result = jsonObject.optJSONObject("result");
                jsonObject_detail = jsonObject_result.optJSONObject("customer_detail");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            progressDialog.dismiss();
            Log.e("onPostExecute", "onPostExecute");
            Toast.makeText(EditProfileActivity.this, jsonObject.optString("msg"), Toast.LENGTH_SHORT).show();
        }

        public byte[] getBitmapAsByteArray(Bitmap bitmap) {
            if (bitmap != null) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 80, outputStream);
                return outputStream.toByteArray();
            } else {
                return null;
            }
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bmp) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void selectimage() {
        String str[] = {"Choose From Gallery", "Open Camera", "Cancel"};
        AlertDialog.Builder alert = new AlertDialog.Builder(EditProfileActivity.this);
        alert.setItems(str,
                new DialogInterface.OnClickListener() {
                    @SuppressLint("NewApi")
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if (which == 0) {
                            // gallery
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                REQUEST_CODE = 70;
                                if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                                    photogallery();
                                } else {
                                    requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
                                }
                            } else {
                                photogallery();
                            }
                        } else if (which == 1) {
                            // camera
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                MY_REQUEST_CODE = 40;
                                if (checkSelfPermission(android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    requestPermissions(new String[]{android.Manifest.permission.CAMERA}, MY_REQUEST_CODE);
                                } else {
                                    photocamera_perm();
                                }
                            } else {
                                photocamera();
                            }
                        } else if (which == 2) {

                        }
                    }

                });
        alert.show();
    }

    @SuppressLint("NewApi")
    private void photocamera_perm() {
        REQUEST_CODE = 50;
        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            photocamera();
        } else {
            requestPermissions(
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_CODE);
        }
    }

    private void photogallery() {
        // TODO Auto-generated method stub
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent();
            intent.setType("image/jpeg");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Image"), 11);
        } else {
            Intent galleryIntent = new Intent(Intent.ACTION_PICK);
            galleryIntent.setType("image/jpeg");
            startActivityForResult(galleryIntent, 33);

        }
    }

    private void photocamera() {
        // TODO Auto-generated method stub
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 11:
                if (resultCode == RESULT_OK) {
                    Log.e("11", "11");
                    Uri selectedImage = data.getData();
                    String path = getPath(EditProfileActivity.this, selectedImage);
                    if (path != null) {
                        mPath = path;
                        try {
                            mBitmap = Utility.decodeSampledBitmap(EditProfileActivity.this, Uri.fromFile(new File(mPath)));
                            Log.e("mBitmap", "" + mBitmap);
                            ExifInterface exif = new ExifInterface(photo.toString());
                            if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")) {
                                mBitmap = rotate(mBitmap, 90);
                            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")) {
                                mBitmap = rotate(mBitmap, 270);
                            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")) {
                                mBitmap = rotate(mBitmap, 180);
                            } else if (exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")) {
                                mBitmap = rotate(mBitmap, 90);
                            }
                        } catch (IOException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                    }

                }
                break;
            case 33:
                if (resultCode == RESULT_OK) {
                    Log.e("33", "33");
                    mTempBitmapArray.clear();
                    selectedPath = "";
                    Uri selectedImageUri = data.getData();
                    try {
                        selectedPath = getImagePath(selectedImageUri);
                        Log.e("selectedPath", "" + selectedPath);
                        try {
                            mBitmap = Utility.decodeSampledBitmap(EditProfileActivity.this, Uri.fromFile(new File(selectedPath)));
                            Log.e("mBitmap", "" + mBitmap);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        profile_image.setImageBitmap(mBitmap);

                        if (Utils.isNetworkAvailable(EditProfileActivity.this)) {
                            new update_profile().execute();
                        } else {
                            Toast.makeText(EditProfileActivity.this, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }
                        //
                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                } else if (resultCode == SELECT_IMAGE) {
                    Uri selectedImage = data.getData();
                    String path = getPath(EditProfileActivity.this, selectedImage);
                    if (path != null) {
                        mPath = path;
                        try {
                            mBitmap = Utility.decodeSampledBitmap(EditProfileActivity.this, Uri.fromFile(new File(mPath)));
                        } catch (IOException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        }
                        image_array.add(mBitmap);
                        profile_image.setImageBitmap(mBitmap);
                    }
                }
                break;
            case 1:
                Log.e("1", "1");
                if (resultCode == RESULT_OK) {
                    mTempBitmapArray.clear();
                    onCaptureImageResult(data);
                }
                break;
        }
    }

    public int getOrientation(Uri selectedImage) {
        int orientation = 0;
        final String[] projection = new String[]{MediaStore.Images.Media.ORIENTATION};
        final Cursor cursor = EditProfileActivity.this.getContentResolver().query(selectedImage, projection, null, null, null);
        if (cursor != null) {
            final int orientationColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.ORIENTATION);
            if (cursor.moveToFirst()) {
                orientation = cursor.isNull(orientationColumnIndex) ? 0 : cursor.getInt(orientationColumnIndex);
            }
            cursor.close();
        }
        return orientation;
    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        mPath = "";
        File destination = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPath = destination.getAbsolutePath();
        Log.e("mPath", "" + mPath);

        try {
            mBitmap = Utility.decodeSampledBitmap(EditProfileActivity.this, Uri.fromFile(new File(mPath)));

        } catch (IOException e) {
            e.printStackTrace();
        }

        profile_image.setImageBitmap(mBitmap);
        if (Utils.isNetworkAvailable(EditProfileActivity.this)) {
            new update_profile().execute();
        } else {
            Toast.makeText(EditProfileActivity.this, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_REQUEST_CODE || requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == 40) {
                    photocamera_perm();
                } else if (requestCode == 50) {
                    photocamera();
                } else if (requestCode == 70) {
                    photogallery();
                }
            } else {
            }
        }
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/"
                            + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection,
                        selectionArgs);
            }
        }
        // MediaStore (and general)

        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri,
                                       String selection, String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private String getImagePath(Uri selectedImageUri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(selectedImageUri.getScheme())) {
            String[] projection = {"_data"};
            Cursor cursor = null;
            try {
                cursor = EditProfileActivity.this.getContentResolver().query(selectedImageUri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ("file".equalsIgnoreCase(selectedImageUri.getScheme())) {
            //return selectedImageUri.getImagePath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    public void uploadPDF(final byte[] b, final int saveImage, String name, String email, String phone, String add_1, String add_2, String city, String pincode) throws JSONException, UnsupportedEncodingException {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        VolleyMultipartRequest stringRequest = new VolleyMultipartRequest(Request.Method.POST, Constants.updateprofile,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        try {
                            String resultResponse = new String(response.data);
                            Log.e("resultResponse", "" + resultResponse);
                            JSONObject jsonobject;
                            jsonobject = new JSONObject(resultResponse);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            if (progressDialog.isShowing())
                                progressDialog.dismiss();

                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                        String message = "";
                        if (volleyError instanceof NetworkError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ServerError) {
                            message = "The server could not be found. Please try again after sometime!!";
                        } else if (volleyError instanceof AuthFailureError) {
                            message = "Cannot connect to Internet...Please check your connection!";
                        } else if (volleyError instanceof ParseError) {
                            message = "Parsing error! Please try again after some time!!";
                        } else if (volleyError instanceof TimeoutError) {
                            message = "Connection TimeOut! Please check your internet connection.";
                        } else {
                            // message = getResources().getString(R.string.internet_error);
                        }
                        //   showSnack(message);
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("name", name);
                params.put("city", city);
                params.put("state", state);
                params.put("mobile", phone);
                params.put("address_line1", add_1);
                params.put("address_line2", add_2);
                params.put("zip", pincode);
                Log.e("params", "" + Constants.updateprofile + params);
                return params;
            }


            @SuppressLint("LongLogTag")
            @Override
            protected Map<String, DataPart> getByteData() throws IOException, URISyntaxException {
                Map<String, DataPart> params = new HashMap<>();
                Log.e("param image_array size", "" + image_array.size());
                for (int i = 0; i < image_array.size(); i++) {
                    try {
                        params.put("signature_to_save_inserver[" + i + "]", new DataPart(System.currentTimeMillis() + "signature_" + i + ".jpg", getFileDataFromDrawable(image_array.get(i))));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                params.put("pdfdata", new DataPart(String.valueOf(System.currentTimeMillis()) + selectedPath, b, "application/image"));
                //  params.put("signature_to_save_inserver", new DataPart(System.currentTimeMillis() + "signature.jpg", getFileDataFromDrawable(bmp)));

                return params;
            }


        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(EditProfileActivity.this).addToRequestQueue(stringRequest);

    }

}