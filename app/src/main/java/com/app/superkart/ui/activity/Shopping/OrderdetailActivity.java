package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.OrderModel;
import com.app.superkart.model.Shopping.ShipModel;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderdetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private TextView txt_orderid, txt_sellerName, txt_TotalAmount, txt_paymentmode;
    private RecyclerView recycle_list;
    private LinearLayoutManager linearLayoutManager;

    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private ArrayList<OrderModel> arrayList = new ArrayList();
    private ArrayList<ShipModel> arrayList_track = new ArrayList();
    private OrderListDeatilAdpter orderListDeatilAdpter;
    private OrderTrackAdpter orderTrackAdpter;
    private String order_id = "", order_id_full = "", payment_mode = "";
    private ArrayList<ShipModel> arrayList_ship = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.order_detail_layout);
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        Log.e("OrderdetailActivity", "OrderdetailActivity");
        init();
        intentData();
        if (Utils.isNetworkAvailable(OrderdetailActivity.this)) {
            orderdetail();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
    }

    public void init() {
        txt_orderid = findViewById(R.id.txt_orderid);
        txt_sellerName = findViewById(R.id.txt_sellerName);
        txt_TotalAmount = findViewById(R.id.txt_TotalAmount);
        txt_paymentmode = findViewById(R.id.txt_paymentmode);
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        linearLayoutManager = new LinearLayoutManager(OrderdetailActivity.this, RecyclerView.VERTICAL, false);
        recycle_list.setLayoutManager(linearLayoutManager);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            order_id_full = (String) bd.get("order_id_full");
            payment_mode = (String) bd.get("payment_mode");
            Log.e("payment_mode", "" + payment_mode);
            txt_orderid.setText("Order ID: " + order_id_full);
            txt_paymentmode.setText(payment_mode);
            if (payment_mode.equals("online")) {
                txt_paymentmode.setTextColor(Color.parseColor("#8fe66e"));
            }

        }
    }

    private void orderdetail() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.orderdetail, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            OrderModel model = new OrderModel();
                            model.setId(array_result.optJSONObject(i).optString("id"));
                            model.setOrder_id(array_result.optJSONObject(i).optString("order_id"));
                            model.setProduct_name(array_result.optJSONObject(i).optString("product_name"));
                            model.setProduct_image(array_result.optJSONObject(i).optString("product_image"));
                            model.setOrder_date(array_result.optJSONObject(i).optString("order_date"));
                            model.setPrice(array_result.optJSONObject(i).optString("price"));
                            model.setObj_shipping(array_result.optJSONObject(i).optString("shipping"));
                            arrayList.add(model);
                        }

                        if (arrayList.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            orderListDeatilAdpter = new OrderListDeatilAdpter(OrderdetailActivity.this, arrayList);
                            recycle_list.setAdapter(orderListDeatilAdpter);
                        } else {
                            recycle_list.setVisibility(View.GONE);
                        }

                        if (json_main.optString("storename").equals("") || json_main.optString("storename").equals("null") || json_main.optString("storename").equals(null)) {
                            txt_sellerName.setText("");
                            txt_sellerName.setVisibility(View.GONE);
                        } else {
                            txt_sellerName.setVisibility(View.VISIBLE);
                            txt_sellerName.setText("Seller: " + json_main.optString("storename"));
                        }

                        if (json_main.optString("total").equals("") || json_main.optString("total").equals("null") || json_main.optString("total").equals(null)) {
                            txt_TotalAmount.setText("");
                        } else {
                            txt_TotalAmount.setText("Total: " + "₹" + json_main.optString("total"));
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("order_id", AppConstant.order_id_deatil);
                Log.e("params", "" + Constants.orderdetail + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.orderdetail);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class OrderListDeatilAdpter extends RecyclerView.Adapter<OrderListDeatilAdpter.MyViewHolder> {

        private List<OrderModel> arrayList;
        private Context context;
        private LinearLayoutManager linearLayoutManager1;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_item;
            private TextView txt_productName, txt_price, txt_Orderdate, txt_sellerName, txt_status;
            private RecyclerView recycle_trackOrder;

            public MyViewHolder(View view) {
                super(view);
                txt_productName = view.findViewById(R.id.txt_productName);
                img_item = view.findViewById(R.id.img_item);
                txt_price = view.findViewById(R.id.txt_price);
                txt_Orderdate = view.findViewById(R.id.txt_Orderdate);
                txt_sellerName = view.findViewById(R.id.txt_sellerName);
                recycle_trackOrder = view.findViewById(R.id.recycle_trackOrder);
                txt_status = view.findViewById(R.id.txt_status);
            }
        }

        public OrderListDeatilAdpter(Context context, List<OrderModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_detail_item_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            OrderModel model = arrayList.get(position);
            if (model.getProduct_name().equals("") | model.getProduct_name().equals("null") || model.getProduct_name().equals(null)) {
                holder.txt_productName.setText("");
            } else {
                holder.txt_productName.setText(model.getProduct_name());
            }
            if (model.getProduct_image().equals("") | model.getProduct_image().equals("null") || model.getProduct_image().equals(null)) {

            } else {
                /*Picasso.with(context)
                        .load(model.getProduct_image())
                        .into(holder.img_item);*/
                Glide
                        .with(context)
                        .load(model.getProduct_image())
                        .centerCrop()
                        .into(holder.img_item);

            }
            if (model.getPrice().equals("") | model.getPrice().equals("null") || model.getPrice().equals(null)) {
                holder.txt_price.setText("");
            } else {
                holder.txt_price.setText("Price: " + "₹" + model.getPrice());
            }

            if (model.getOrder_date().equals("") | model.getOrder_date().equals("null") || model.getOrder_date().equals(null)) {
                holder.txt_Orderdate.setText("");
            } else {
                holder.txt_Orderdate.setText("Order Date: " + model.getOrder_date());
            }


            try {
                arrayList_ship.clear();
                JSONObject obj_ship = new JSONObject(model.getObj_shipping());
                JSONArray array = obj_ship.optJSONArray("shippment_data");
                for (int i = 0; i < array.length(); i++) {
                    ShipModel shipModel = new ShipModel();
                    shipModel.setOrder_status(array.optJSONObject(i).optString("order_status"));
                    shipModel.setShippment_status(array.optJSONObject(i).optString("shippment_status"));
                    shipModel.setDate(array.optJSONObject(i).optString("date"));
                    shipModel.setTime(array.optJSONObject(i).optString("time"));
                    shipModel.setMsg(array.optJSONObject(i).optString("msg"));
                    arrayList_ship.add(shipModel);
                }
                if (arrayList_ship.size() > 0) {
                    linearLayoutManager1 = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
                    holder.recycle_trackOrder.setLayoutManager(linearLayoutManager1);
                    orderTrackAdpter = new OrderTrackAdpter(OrderdetailActivity.this, arrayList_ship);
                    holder.recycle_trackOrder.setAdapter(orderTrackAdpter);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

    public class OrderTrackAdpter extends RecyclerView.Adapter<OrderTrackAdpter.MyViewHolder> {

        private List<ShipModel> arrayList;
        private Context context;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private ImageView img_status, txt_line;
            private TextView txt_status, txt_date;

            public MyViewHolder(View view) {
                super(view);
                img_status = view.findViewById(R.id.img_status);
                txt_line = view.findViewById(R.id.txt_line);
                txt_status = view.findViewById(R.id.txt_status);
                txt_date = view.findViewById(R.id.txt_date);
            }
        }

        public OrderTrackAdpter(Context context, List<ShipModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.order_track_order_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ShipModel model = arrayList.get(position);

            if (model.getOrder_status().equals("Accepted")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Process")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Packed")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Shipped")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setBackgroundResource(R.drawable.line_green);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            } else if (model.getOrder_status().equals("Delivered")) {
                holder.img_status.setBackgroundResource(R.drawable.check_ok);
                holder.txt_line.setVisibility(View.INVISIBLE);
                holder.txt_status.setText(model.getShippment_status());
                holder.txt_date.setText(model.getTime() + " " + model.getDate());
            }

        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }
    }

}
