package com.app.superkart.ui.activity.Food;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.adpters.Food.FoodDetailAdpter;
import com.app.superkart.adpters.Service.Home_Visit_Palce_Adpter;
import com.app.superkart.ui.activity.Service.PalaceOrder_HomeVisitActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.Utils;

import java.util.ArrayList;

public class FoodDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private RecyclerView recycle_item;
    private FoodDetailAdpter foodDetailAdpter;
    private ArrayList<String> arrayList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private ImageView img_back;
    private TextView btn_bookTable, btn_Online;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.food_detail_layout);
        Log.e("FoodDetailActivity", "FoodDetailActivity");
        init();
        for (int i = 0; i < 5; i++) {
            arrayList.add("a");
        }

        recycle_item = findViewById(R.id.recycle_item);
        linearLayoutManager = new LinearLayoutManager(FoodDetailActivity.this, LinearLayoutManager.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);
        foodDetailAdpter = new FoodDetailAdpter(FoodDetailActivity.this, arrayList);
        recycle_item.setAdapter(foodDetailAdpter);
    }

    public void init() {
        btn_bookTable = findViewById(R.id.btn_bookTable);
        btn_Online = findViewById(R.id.btn_Online);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
        btn_bookTable.setOnClickListener(this);
        btn_Online.setOnClickListener(this);
        btn_bookTable.setBackgroundResource(R.drawable.boc_corner);
        btn_Online.setBackgroundResource(R.drawable.bg_corner_yellow);
        btn_bookTable.setTextColor(Color.parseColor("#FF000000"));
        btn_Online.setTextColor(Color.parseColor("#FFFFFFFF"));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                AppConstant.check_calender_view = "";
                finish();
                break;
            case R.id.btn_bookTable:
                AppConstant.check_calender_view = "food";
                btn_bookTable.setBackgroundResource(R.drawable.bg_corner_yellow);
                btn_Online.setBackgroundResource(R.drawable.boc_corner);
                btn_bookTable.setTextColor(Color.parseColor("#FFFFFFFF"));
                btn_Online.setTextColor(Color.parseColor("#FF000000"));
                Intent i = new Intent(FoodDetailActivity.this, BooKTableActivity.class);
                startActivity(i);
                break;
            case R.id.btn_Online:
                AppConstant.check_calender_view = "";
                btn_bookTable.setBackgroundResource(R.drawable.boc_corner);
                btn_Online.setBackgroundResource(R.drawable.bg_corner_yellow);
                btn_bookTable.setTextColor(Color.parseColor("#FF000000"));
                btn_Online.setTextColor(Color.parseColor("#FFFFFFFF"));
                break;
        }
    }
}
