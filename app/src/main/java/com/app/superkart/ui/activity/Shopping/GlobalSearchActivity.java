package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.ProductListModel;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GlobalSearchActivity extends AppCompatActivity {
    private Context mcontext;
    private ProgressDialog progressDialog;
    private ArrayList<ProductListModel> arrayList_item = new ArrayList<>();
    private ItemSeachAdpter itemSeachAdpter;
    private RecyclerView recycle_list;
    private LinearLayoutManager linearLayoutManager;
    private EditText et_global_search;
    private ImageView img_cancel, img_back;
    private String search_keyword = "",Status="";
    private TextView txt_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.global_serach_list_layout);
        Log.e("GlobalSearchActivity", "GlobalSearchActivity");
        progressDialog = new ProgressDialog(GlobalSearchActivity.this, R.style.AppCompatAlertDialogStyle);
        recycle_list = findViewById(R.id.recycle_list);
        linearLayoutManager = new LinearLayoutManager(GlobalSearchActivity.this, RecyclerView.VERTICAL, false);
        recycle_list.setLayoutManager(linearLayoutManager);
        et_global_search = findViewById(R.id.et_global_search);

        img_cancel = findViewById(R.id.img_cancel);
        img_back = findViewById(R.id.img_back);
        txt_msg = findViewById(R.id.txt_msg);

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_global_search.setText("");
                arrayList_item.clear();
                txt_msg.setVisibility(View.GONE);
            }
        });
        et_global_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    if (Utils.isNetworkAvailable(GlobalSearchActivity.this)) {
                        search_keyword = s.toString();
                        if (search_keyword.length() > 3) {
                            gb_search();
                        }


                    } else {
                        Toast.makeText(GlobalSearchActivity.this, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    arrayList_item.clear();
                    recycle_list.setVisibility(View.GONE);
                    Utils.hideKeyboard(GlobalSearchActivity.this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_global_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(et_global_search.getWindowToken(), 0);
                    search_keyword = et_global_search.getText().toString().trim();
                    Utils.hideKeyboard(GlobalSearchActivity.this);
                    Intent i = new Intent(GlobalSearchActivity.this, ItemSearchListActivity.class);
                    i.putExtra("keyword", search_keyword);
                    i.putExtra("sl", "");
                    i.putExtra("el", "");
                    startActivity(i);
                    return true;
                }
                return false;
            }
        });
    }

    private void gb_search() {
        String tag_string_req = "req";
        // progressDialog.setMessage("Please Wait...");
        // progressDialog.setCancelable(false);
        //   progressDialog.show();
        arrayList_item.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.gb_search, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                /*if (progressDialog.isShowing())
                    progressDialog.dismiss();*/
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        Utils.hideKeyboard(GlobalSearchActivity.this);
                        JSONArray array_data = json_main.optJSONArray("data_result");
                        for (int i = 0; i < array_data.length(); i++) {
                            ProductListModel model = new ProductListModel();
                            model.setProduct_id(array_data.optJSONObject(i).optString("serch_res_id"));
                            model.setProduct_name(array_data.optJSONObject(i).optString("serch_res_name"));
                            model.setItem_type(array_data.optJSONObject(i).optString("item_type"));
                            model.setSeller_name(array_data.optJSONObject(i).optString("store_name"));
                            model.setSelling_type(array_data.optJSONObject(i).optString("selling_type"));
                            model.setIs_fav(array_data.optJSONObject(i).optString("is_fav"));
                            arrayList_item.add(model);
                        }

                        if (arrayList_item.size() > 0) {
                            recycle_list.setVisibility(View.VISIBLE);
                            txt_msg.setVisibility(View.GONE);
                            itemSeachAdpter = new ItemSeachAdpter(GlobalSearchActivity.this, arrayList_item);
                            recycle_list.setAdapter(itemSeachAdpter);
                            itemSeachAdpter.notifyDataSetChanged();
                        } else {
                            //  progressDialog.dismiss();
                            Utils.hideKeyboard(GlobalSearchActivity.this);
                            recycle_list.setVisibility(View.GONE);
                            txt_msg.setVisibility(View.VISIBLE);
                        }
                    } else {
                        //progressDialog.dismiss();
                        Utils.hideKeyboard(GlobalSearchActivity.this);
                        txt_msg.setVisibility(View.VISIBLE);
                        txt_msg.setText(json_main.optString("msg"));
                        //  Toast.makeText(GlobalSearchActivity.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    // progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
               /* if (progressDialog.isShowing())
                    progressDialog.dismiss();*/
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("keyword", search_keyword);
                Log.e("params", "" + Constants.gb_search + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.gb_search);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class ItemSeachAdpter extends RecyclerView.Adapter<ItemSeachAdpter.MyViewHolder> {

        private List<ProductListModel> arrayList;
        private Context context;
        private int pos = 0;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_itemName, txt_seller_name;
            private LinearLayout li_main;

            public MyViewHolder(View view) {
                super(view);
                txt_itemName = view.findViewById(R.id.txt_itemName);
                li_main = view.findViewById(R.id.li_main);
                txt_seller_name = view.findViewById(R.id.txt_seller_name);

            }
        }

        public ItemSeachAdpter(Context context, List<ProductListModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.global_serach_list_adpter, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            ProductListModel model = arrayList.get(position);

            if (arrayList.get(position).getProduct_name().equals("") || arrayList.get(position).getProduct_name().equals("null") || arrayList.get(position).getProduct_name().equals(null)) {

            } else {
                holder.txt_itemName.setText(arrayList.get(position).getProduct_name());
            }
            if (arrayList.get(position).getSeller_name().equals("") || arrayList.get(position).getSeller_name().equals("null") || arrayList.get(position).getSeller_name().equals(null)) {

            } else {
                holder.txt_seller_name.setText(arrayList.get(position).getSeller_name());
            }

            holder.li_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.hideKeyboard(GlobalSearchActivity.this);
                    Intent i = new Intent(GlobalSearchActivity.this, ItemSearchListActivity.class);
                    i.putExtra("keyword", search_keyword);
                    i.putExtra("sl", "");
                    i.putExtra("el", "");
                    context.startActivity(i);
                }
            });


        }


        @Override
        public int getItemCount() {
            return arrayList.size();
        }


    }

    @Override
    public void onBackPressed() {
        Utils.hideKeyboard(GlobalSearchActivity.this);
        finish();
        super.onBackPressed();
    }
}
