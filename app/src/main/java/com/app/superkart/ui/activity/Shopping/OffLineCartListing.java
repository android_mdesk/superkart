package com.app.superkart.ui.activity.Shopping;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.CartModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.fragment.CartFragment;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OffLineCartListing extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private ImageView img_back;
    private RelativeLayout rl_cart;
    private TextView txt_cart_count, txt_amount, txt_popup_title, txt_btn_popup_loyalty, txt_loyalty, txt_btn_reddemNow, txt_loyalty_new, txt_btn_continue;
    private RecyclerView recycle_item;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout li_btn_placeOrder, li_error_msg, li_loyalty;
    private RelativeLayout rel_loyalty_popup;
    private ImageView img_popclick, img_cancel, img_1;
    private EditText et_pop_loyalty;
    private ArrayList<CartModel> arrayList = new ArrayList<>();
    private FashionCartAdpter fashionCartAdpter;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    private ArrayList<String> array_qty = new ArrayList<>();
    private CardView card_redeem;
    private String vender_id = "", LoyaltyPoints = "", totalamt = "";
    private View view_line;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.offline_cart_listing_layout);
        Log.e("OffLineCartListing", "OffLineCartListing");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
            Toast.makeText(OffLineCartListing.this, "Please login to view your cart data.", Toast.LENGTH_SHORT).show();
        } else {
            if (Utils.isNetworkAvailable(OffLineCartListing.this)) {
                cartlistoffline();
            } else {
                Toast.makeText(OffLineCartListing.this, R.string.check_internet, Toast.LENGTH_LONG).show();
            }
        }
        for (int i = 1; i < 11; i++) {
            array_qty.add(String.valueOf(i));
        }
    }

    public void init() {

        img_back = findViewById(R.id.img_back);
        rl_cart = findViewById(R.id.rl_cart);
        txt_cart_count = findViewById(R.id.txt_cart_count);
        txt_amount = findViewById(R.id.txt_amount);
        recycle_item = findViewById(R.id.recycle_item);
        li_btn_placeOrder = findViewById(R.id.li_btn_placeOrder);
        li_error_msg = findViewById(R.id.li_error_msg);
        rel_loyalty_popup = findViewById(R.id.rel_loyalty_popup);
        img_popclick = findViewById(R.id.img_popclick);
        txt_popup_title = findViewById(R.id.txt_popup_title);
        img_cancel = findViewById(R.id.img_cancel);
        et_pop_loyalty = findViewById(R.id.et_pop_loyalty);
        txt_btn_popup_loyalty = findViewById(R.id.txt_btn_popup_loyalty);
        txt_loyalty = findViewById(R.id.txt_loyalty);
        li_loyalty = findViewById(R.id.li_loyalty);
        card_redeem = findViewById(R.id.card_redeem);
        txt_btn_reddemNow = findViewById(R.id.txt_btn_reddemNow);
        view_line = findViewById(R.id.view_line);
        txt_loyalty_new = findViewById(R.id.txt_loyalty_new);
        img_1 = findViewById(R.id.img_1);
        txt_btn_continue = findViewById(R.id.txt_btn_continue);

        linearLayoutManager = new LinearLayoutManager(OffLineCartListing.this, RecyclerView.VERTICAL, false);
        recycle_item.setLayoutManager(linearLayoutManager);

        txt_popup_title.setText("Redeem your loyalty points");
        txt_btn_popup_loyalty.setText("Redeem Now");

        img_back.setOnClickListener(this);
        img_popclick.setOnClickListener(this);
        img_cancel.setOnClickListener(this);
        txt_btn_popup_loyalty.setOnClickListener(this);
        card_redeem.setOnClickListener(this);
        txt_btn_continue.setOnClickListener(this);
        rl_cart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
            case R.id.img_popclick:
                break;
            case R.id.img_cancel:
                rel_loyalty_popup.setVisibility(View.GONE);
                break;
            case R.id.txt_btn_popup_loyalty:
                String new_Lp = et_pop_loyalty.getText().toString().trim();
                cartReedemRequest(vender_id, LoyaltyPoints, new_Lp);
                break;
            case R.id.card_redeem:
                rel_loyalty_popup.setVisibility(View.VISIBLE);
                break;
            case R.id.txt_btn_continue:

                if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                    Toast.makeText(OffLineCartListing.this, "Please login to view your cart data.", Toast.LENGTH_SHORT).show();
                } else {
                    if (Utils.isNetworkAvailable(OffLineCartListing.this)) {
                        orderoffline();
                    } else {
                        Toast.makeText(OffLineCartListing.this, R.string.check_internet, Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    public void cartlistoffline() {
        String tag_string_req = "req";
        progressDialog = new ProgressDialog(OffLineCartListing.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cartlistoffline, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {

                        if (json_main.optString("remainingLp").equals("0") || json_main.optString("remainingLp").equals("null") || json_main.optString("remainingLp").equals("") || json_main.optString("remainingLp").equals(null)) {
                            txt_loyalty_new.setVisibility(View.GONE);
                            img_1.setVisibility(View.GONE);
                        } else {
                            txt_loyalty_new.setVisibility(View.VISIBLE);
                            img_1.setVisibility(View.VISIBLE);
                            txt_loyalty_new.setText(json_main.optString("remainingLp"));
                        }

                        if (json_main.optString("loyaltyPoints").equals("0") || json_main.optString("loyaltyPoints").equals("null") || json_main.optString("loyaltyPoints").equals("") || json_main.optString("loyaltyPoints").equals(null)) {
                            li_loyalty.setVisibility(View.GONE);
                            view_line.setVisibility(View.GONE);
                            txt_loyalty.setText(json_main.optString("loyaltyPoints"));
                            LoyaltyPoints = json_main.optString("loyaltyPoints");
                            txt_btn_reddemNow.setBackgroundResource(R.drawable.card_view_bg_gray);
                            txt_btn_reddemNow.setEnabled(false);
                        } else {
                            view_line.setVisibility(View.VISIBLE);
                            li_loyalty.setVisibility(View.VISIBLE);
                            txt_loyalty.setText(json_main.optString("loyaltyPoints"));
                            LoyaltyPoints = json_main.optString("loyaltyPoints");
                            txt_btn_reddemNow.setBackgroundResource(R.drawable.card_view_bg);
                            txt_btn_reddemNow.setEnabled(true);
                        }


                        JSONArray array_result = json_main.optJSONArray("result");
                        for (int i = 0; i < array_result.length(); i++) {
                            CartModel model = new CartModel();
                            vender_id = array_result.optJSONObject(i).optString("vendor_id");
                            model.setCart_id(array_result.optJSONObject(i).optString("cart_id"));
                            model.setProduct_id(array_result.optJSONObject(i).optString("product_id"));
                            model.setProduct_name(array_result.optJSONObject(i).optString("product_name"));
                            model.setProduct_image(array_result.optJSONObject(i).optString("product_image"));
                            model.setRegular_price(array_result.optJSONObject(i).optString("regular_price"));
                            model.setSale_price(array_result.optJSONObject(i).optString("sale_price"));
                            model.setPrice_off(array_result.optJSONObject(i).optString("priceoff"));
                            model.setQty(array_result.optJSONObject(i).optString("qty"));
                            model.setDescription(array_result.optJSONObject(i).optString("description"));
                            model.setIs_fav(array_result.optJSONObject(i).optString("is_fav"));
                            arrayList.add(model);
                        }

                        if (arrayList.size() > 0) {
                            li_error_msg.setVisibility(View.GONE);
                            txt_cart_count.setVisibility(View.VISIBLE);
                            txt_cart_count.setText(json_main.optString("totalCart"));
                            txt_amount.setVisibility(View.VISIBLE);
                            li_btn_placeOrder.setVisibility(View.VISIBLE);
                            recycle_item.setVisibility(View.VISIBLE);
                            txt_amount.setText("₹" + json_main.optString("totalamt"));
                            totalamt = json_main.optString("totalamt");
                            AppConstant.total_price = json_main.optString("totalamt");

                            fashionCartAdpter = new FashionCartAdpter(OffLineCartListing.this, arrayList);
                            recycle_item.setAdapter(fashionCartAdpter);
                        } else {
                            txt_cart_count.setVisibility(View.GONE);
                            txt_amount.setVisibility(View.GONE);
                            li_btn_placeOrder.setVisibility(View.GONE);
                            recycle_item.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }

                    } else {
                        progressDialog.dismiss();
                        li_error_msg.setVisibility(View.VISIBLE);
                        recycle_item.setVisibility(View.GONE);
                        Toast.makeText(OffLineCartListing.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("seller_id", AppConstant.seller_id);
                Log.e("params", "" + Constants.cartlistoffline + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cartlistoffline);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class FashionCartAdpter extends RecyclerView.Adapter<FashionCartAdpter.MyViewHolder> {

        private List<CartModel> arrayList;
        private Context context;
        private String[] qnty;
        private String qty = "";
        int index = 0;
        private SharedPreferences preferences;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView img_item;
            TextView txt_itemName, txt_item_Desc, txt_salePrice, txt_RegularPrice, txt_priceOff, txt_remove, txt_whiistList, txt_minus, txt_value, txt_pulse;
            Spinner spinner_Qty;

            public MyViewHolder(View view) {
                super(view);
                img_item = view.findViewById(R.id.img_item);
                txt_itemName = view.findViewById(R.id.txt_itemName);
                txt_item_Desc = view.findViewById(R.id.txt_item_Desc);
                txt_salePrice = view.findViewById(R.id.txt_salePrice);
                txt_RegularPrice = view.findViewById(R.id.txt_RegularPrice);
                txt_priceOff = view.findViewById(R.id.txt_priceOff);
                spinner_Qty = view.findViewById(R.id.spinner_Qty);
                txt_remove = view.findViewById(R.id.txt_remove);
                txt_whiistList = view.findViewById(R.id.txt_whiistList);
                txt_minus = view.findViewById(R.id.txt_minus);
                txt_value = view.findViewById(R.id.txt_value);
                txt_pulse = view.findViewById(R.id.txt_pulse);

            }
        }

        public FashionCartAdpter(Context context, List<CartModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adpter_fashion_cart, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            CartModel model = arrayList.get(position);
            preferences = context.getSharedPreferences(context.getString(R.string.app_name), Context.MODE_PRIVATE);

            if (model.getProduct_image().equals("")) {

            } else {
              /*  Picasso.with(context)
                        .load(model.getProduct_image())
                        .into(holder.img_item);*/

                Glide
                        .with(context)
                        .load(model.getProduct_image())
                        .centerCrop()
                        .into(holder.img_item);
            }

            if (model.getProduct_name().equals("")) {

            } else {
                holder.txt_itemName.setText(model.getProduct_name());
            }

            if (model.getSale_price().equals("")) {

            } else {
                holder.txt_salePrice.setText("₹" + model.getSale_price());
            }

            if (model.getRegular_price().equals("")) {

            } else {
                holder.txt_RegularPrice.setText("₹" + model.getRegular_price());
            }

            if (model.getPrice_off().equals("")) {

            } else {
                holder.txt_priceOff.setText("RS " + model.getPrice_off() + " OFF");
            }

            if (model.getDescription().equals("")) {

            } else {
                holder.txt_item_Desc.setText(model.getDescription());
            }

            if (model.getIs_fav().equals("1")) {
                holder.txt_whiistList.setTextColor(Color.parseColor("#FFA800"));
            } else {
                holder.txt_whiistList.setTextColor(Color.parseColor("#9e9e9e"));
            }

            holder.txt_whiistList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                        Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                    } else {
                        if (model.getIs_fav().equals("0")) {
                            addtowishlist(model.getProduct_id());
                        } else {
                            removewishlist(model.getProduct_id());
                        }
                    }
                }
            });

            holder.txt_remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(OffLineCartListing.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("Do you want to remove this product from cart?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                                Utils.showSnackBar(v, "Please Login to add this product whistlist.");
                            } else {
                                removeItem(model.getCart_id());
                            }
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                            //dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.black));
                        }
                    });
                    alert.show();

                }
            });


            if (model.getQty().equals("")) {

            } else {
                holder.txt_value.setText(model.getQty());
            }
            holder.txt_minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.txt_value.setText(model.getQty());
                }
            });

            holder.txt_pulse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.txt_value.setText(model.getQty());
                }
            });

           /* for (int j = 0; j < arrayList.size(); j++) {
                array_qty.add(arrayList.get(j).getQty());
            }*/
            for (int i = 0; i < array_qty.size(); i++) {
                if (array_qty.get(i).equals(model.getQty())) {
                    index = i;
                }
            }

            ArrayAdapter<String> adptGenter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array_qty);
            adptGenter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            holder.spinner_Qty.setAdapter(adptGenter);
            holder.spinner_Qty.setSelection(index);

            holder.spinner_Qty.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                    ((TextView) view).setTextColor(Color.BLACK);
                    qty = holder.spinner_Qty.getSelectedItem().toString();
                    Log.e("qty", "" + qty);
                    if (qty.equals(model.getQty())) {
                        AppConstant.check_cart_view = "";
                    } else {
                        AppConstant.check_cart_view = "view";
                        if (preferences.getString(PreferenceManager.user_id, "").equals("")) {
                            Utils.showSnackBar(view, "Please Login to add this product whistlist.");
                        } else {
                            if (AppConstant.check_cart_view.equals("view")) {
                                updatecart(model.getProduct_id(), qty);
                            }
                        }
                    }

                }

                public void onNothingSelected(AdapterView<?> parent) {
                }

            });
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        private void addtowishlist(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.addtowishlist, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            cartlistoffline();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.productlist + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.addtowishlist);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void removewishlist(String product_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.removewishlist, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            cartlistoffline();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.productlist + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.removewishlist);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void removeItem(String Cart_id) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.deletecartoffline, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("status").equalsIgnoreCase("1")) {
                            Toast.makeText(OffLineCartListing.this, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                            cartlistoffline();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("cid", Cart_id);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.deletecartoffline + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.deletecartoffline);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }

        private void updatecart(String product_id, String qty) {
            String tag_string_req = "req";
            progressDialog.setMessage("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            arrayList.clear();
            final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.updatecartoffline, new Response.Listener<String>() {
                @Override
                public void onResponse(final String response) {
                    Log.e("response", "" + response);
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    try {
                        progressDialog.dismiss();
                        JSONObject json_main = new JSONObject(response);
                        if (json_main.optString("RESULT").equalsIgnoreCase("1")) {
                            Toast.makeText(OffLineCartListing.this, json_main.optString("MSG"), Toast.LENGTH_SHORT).show();
                            cartlistoffline();
                        } else {
                            Toast.makeText(context, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressDialog.dismiss();
                    }
                }

            }, new Response.ErrorListener() {
                public void onErrorResponse(VolleyError error) {
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("product_id", product_id);
                    params.put("qty", qty);
                    params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                    Log.e("params", "" + Constants.updatecartoffline + params);
                    return params;
                }
            };
            strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().getRequestQueue().getCache().remove(Constants.updatecartoffline);
            AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
        }
    }

    private void cartReedemRequest(String v_id, String previousloyalty, String loyaltyPoints) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.cartReedemRequest, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        rel_loyalty_popup.setVisibility(View.GONE);
                        Toast.makeText(OffLineCartListing.this, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                        if (Utils.isNetworkAvailable(OffLineCartListing.this)) {
                            cartlistoffline();
                        } else {
                            Toast.makeText(OffLineCartListing.this, R.string.check_internet, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(OffLineCartListing.this, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("seller_id", v_id);
                params.put("previousloyalty", previousloyalty);
                params.put("cart_type", "offline");
                params.put("loyaltyPoints", loyaltyPoints);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.cartReedemRequest + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.cartReedemRequest);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void orderoffline() {
        String tag_string_req = "req";
        progressDialog = new ProgressDialog(OffLineCartListing.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        arrayList.clear();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.orderoffline, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        AppConstant.order_id = json_main.optString("orderId");
                        OrderConfirm_Dailog1(AppConstant.order_id);
                    } else {
                        progressDialog.dismiss();
                        li_error_msg.setVisibility(View.VISIBLE);
                        recycle_item.setVisibility(View.GONE);
                        Toast.makeText(OffLineCartListing.this, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("total_price", totalamt);
                Log.e("params", "" + Constants.orderoffline + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.orderoffline);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void OrderConfirm_Dailog1(String order_id) {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_dailog_alert);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        TextView txt_dialog_orderid = (TextView) dialog.findViewById(R.id.txt_dialog_orderid);
        TextView txt_email = (TextView) dialog.findViewById(R.id.txt_email);
        TextView btn_home = (TextView) dialog.findViewById(R.id.btn_home);
        TextView txt_order_history = (TextView) dialog.findViewById(R.id.txt_order_history);
        TextView txt_status = (TextView) dialog.findViewById(R.id.txt_status);
        TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        TextView txt_amount = (TextView) dialog.findViewById(R.id.txt_amount);
        ImageView imageView5 = (ImageView) dialog.findViewById(R.id.imageView5);
        LinearLayout li_content = (LinearLayout) dialog.findViewById(R.id.li_content);
        txt_dialog_orderid.setText("Order Id:" + order_id);
        txt_amount.setText("Your order is placed " + "₹" + AppConstant.total_price);
        txt_email.setText(preferences.getString(PreferenceManager.email_id, ""));
        imageView5.setBackgroundResource(R.drawable.check_round);

        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                // change.onhome();
                SharedPreferences cart_count_offine = getSharedPreferences("cart_offinr_count", 0);
                cart_count_offine.edit().clear().commit();
                Intent intent = new Intent(OffLineCartListing.this, BaseActivity.class);
                startActivity(intent);
                finish();
            }
        });
        txt_order_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstant.chk_payment_view = "check";
                SharedPreferences cart_count_offine = getSharedPreferences("cart_offinr_count", 0);
                cart_count_offine.edit().clear().commit();
                Intent intent = new Intent(OffLineCartListing.this, MyOrderListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        dialog.show();
    }

}
