package com.app.superkart.ui.activity.Shopping;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.adpters.Shopping.Home_Coupon_Adpter;
import com.app.superkart.adpters.Shopping.SellerForYou_Detail_Adpter;
import com.app.superkart.model.Shopping.OfferModel;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SellerForyouDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private RecyclerView recycle_item, recycle_copuon;
    private LinearLayoutManager linearLayoutManager;
    private GridLayoutManager gridLayoutManager;
    private SellerForYou_Detail_Adpter sellerForYou_detail_adpter;
    private ArrayList<SellerForyouModel> arrayList = new ArrayList<>();
    private ImageView img_back;
    private ProgressDialog progressDialog;
    String seller_id = "", seller_image = "";
    private ImageView img_bg;
    private LinearLayout li_productlist, li_coupon;
    private TextView txt_sellerName, txt_city, txt_sellerName_1, txt_address, txt_phone, txt_time, txt_rating;
    private Home_Coupon_Adpter home_coupon_adpter;
    private ArrayList<OfferModel> arrayList_Recoupon = new ArrayList<>();
    private RatingBar rating;
    private SharedPreferences preferences;

    @SuppressLint("LongLogTag")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle(window, mcontext);
        setContentView(R.layout.seller_for_you_detail_layout);
        Log.e("SellerForyouDetailActivity", "SellerForyouDetailActivity");
        preferences = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
        intentData();
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_item = findViewById(R.id.recycle_item);
        gridLayoutManager = new GridLayoutManager(SellerForyouDetailActivity.this, 2);
        recycle_item.setLayoutManager(gridLayoutManager);

        recycle_copuon = findViewById(R.id.recycle_copuon);
        linearLayoutManager = new LinearLayoutManager(SellerForyouDetailActivity.this, RecyclerView.HORIZONTAL, false);
        recycle_copuon.setLayoutManager(linearLayoutManager);

        img_bg = findViewById(R.id.img_bg);
        li_productlist = findViewById(R.id.li_productlist);
        li_coupon = findViewById(R.id.li_coupon);
        txt_sellerName = findViewById(R.id.txt_sellerName);
        txt_sellerName_1 = findViewById(R.id.txt_sellerName_1);
        txt_address = findViewById(R.id.txt_address);
        txt_city = findViewById(R.id.txt_city);
        txt_phone = findViewById(R.id.txt_phone);
        txt_time = findViewById(R.id.txt_time);
        rating = findViewById(R.id.rating);
        txt_rating = findViewById(R.id.txt_rating);
        img_back = findViewById(R.id.img_back);
        img_back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                finish();
                break;
        }
    }

    public void intentData() {
        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if (bd != null) {
            seller_id = (String) bd.get("seller_id");

        }
    }


    private void sellerdetail() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.sellerdetail, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    progressDialog.dismiss();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("msg").equalsIgnoreCase("Success")) {
                        JSONObject json_result = json_main.optJSONObject("result");
                        if ((json_result.optString("seller_name").equals(""))) {
                        } else {
                            txt_sellerName.setText(json_result.optString("seller_name"));
                        }
                        JSONArray array_venderInfo = json_result.optJSONArray("vendor_info");
                        for (int i = 0; i < array_venderInfo.length(); i++) {
                            if (array_venderInfo.optJSONObject(i).optString("trade_name").equals("") || array_venderInfo.optJSONObject(i).optString("trade_name").equals("null") || array_venderInfo.optJSONObject(i).optString("trade_name").equals(null)) {
                                txt_sellerName_1.setText("");
                            } else {
                                txt_sellerName_1.setText(array_venderInfo.optJSONObject(i).optString("trade_name"));
                            }
                            if (array_venderInfo.optJSONObject(i).optString("seller_image").equals("")) {
                                if (seller_image.equals("")) {

                                } else {
                                    Picasso.with(mcontext)
                                            .load(seller_image)
                                            .into(img_bg);
                                }
                            } else {
                                Picasso.with(mcontext)
                                        .load(array_venderInfo.optJSONObject(i).optString("seller_image"))
                                        .into(img_bg);
                            }
                            txt_rating.setText(array_venderInfo.optJSONObject(i).optString("rating") + ".0");
                            rating.setRating(array_venderInfo.optJSONObject(i).optInt("rating"));
                            txt_time.setText(array_venderInfo.optJSONObject(i).optString("time"));
                            txt_address.setText(array_venderInfo.optJSONObject(i).optString("street") + "\n" + array_venderInfo.optJSONObject(i).optString("landmark") + ",\n" + array_venderInfo.optJSONObject(i).optString("city") + array_venderInfo.optJSONObject(i).optString("state") + array_venderInfo.optJSONObject(i).optString("pin"));
                            txt_city.setText(array_venderInfo.optJSONObject(i).optString("seller_city") + array_venderInfo.optJSONObject(i).optString("seller_state"));
                            txt_phone.setText(array_venderInfo.optJSONObject(i).optString("c_phone"));
                        }
                        if (array_venderInfo.optJSONObject(0).optString("trade_name").equals("") || array_venderInfo.optJSONObject(0).optString("trade_name").equals("null") || array_venderInfo.optJSONObject(0).optString("trade_name").equals(null)) {
                            txt_sellerName_1.setText(json_result.optString("seller_name"));
                        }
                        JSONArray array_vendor_products = json_result.optJSONArray("vendor_products");
                        JSONArray array_coupon = json_result.optJSONArray("vendor_coupons");
                        BindProduct(array_vendor_products);
                        BindCoupom(array_coupon);
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("seller_id", seller_id);
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.sellerdetail + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.sellerdetail);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void BindProduct(JSONArray array_vendor_products) {
        arrayList.clear();
        for (int i = 0; i < array_vendor_products.length(); i++) {
            SellerForyouModel model = new SellerForyouModel();
            model.setProduct_id(array_vendor_products.optJSONObject(i).optString("product_id"));
            model.setProduct_name(array_vendor_products.optJSONObject(i).optString("product_name"));
            model.setProduct_image(array_vendor_products.optJSONObject(i).optString("product_image"));
            model.setStorename(array_vendor_products.optJSONObject(i).optString("storename"));
            model.setSelling_type(array_vendor_products.optJSONObject(i).optString("selling_type"));
            arrayList.add(model);
        }
        if (arrayList.size() > 0) {
            li_productlist.setVisibility(View.VISIBLE);
            sellerForYou_detail_adpter = new SellerForYou_Detail_Adpter(SellerForyouDetailActivity.this, arrayList);
            recycle_item.setAdapter(sellerForYou_detail_adpter);
        } else {
            li_productlist.setVisibility(View.GONE);
        }
    }

    public void BindCoupom(JSONArray array_coupon) {
        arrayList_Recoupon.clear();
        for (int j = 0; j < array_coupon.length(); j++) {
            OfferModel offerModel = new OfferModel();
            offerModel.setCoupon_id(array_coupon.optJSONObject(j).optString("Coupon_Id"));
            offerModel.setCoupon_image(array_coupon.optJSONObject(j).optString("image"));
            offerModel.setCoupon_code(array_coupon.optJSONObject(j).optString("Coupon_Code"));
            arrayList_Recoupon.add(offerModel);
        }
        if (arrayList_Recoupon.size() > 0) {
            li_coupon.setVisibility(View.VISIBLE);
            home_coupon_adpter = new Home_Coupon_Adpter(SellerForyouDetailActivity.this, arrayList_Recoupon);
            recycle_copuon.setAdapter(home_coupon_adpter);
        } else {
            li_coupon.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        if (Utils.isNetworkAvailable(SellerForyouDetailActivity.this)) {
            sellerdetail();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        super.onResume();
    }

}