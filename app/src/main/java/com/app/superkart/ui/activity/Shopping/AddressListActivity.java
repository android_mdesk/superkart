package com.app.superkart.ui.activity.Shopping;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.app.superkart.R;
import com.app.superkart.model.Shopping.AddressModel;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.BaseActivity;
import com.app.superkart.ui.activity.Signup.LoginActivity;
import com.app.superkart.utils.AppConstant;
import com.app.superkart.utils.AppController;
import com.app.superkart.utils.Constants;
import com.app.superkart.utils.PreferenceManager;
import com.app.superkart.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddressListActivity extends AppCompatActivity implements View.OnClickListener {
    private Context mcontext;
    private TextView txt_add;
    private ImageView img_back;
    private RecyclerView recycle_addressList;
    private LinearLayoutManager linearLayoutManager;
    private AddressListAdpter addressListAdpter;
    private ProgressDialog progressDialog;
    private SharedPreferences preferences;
    ArrayList<AddressModel> array_addresslist = new ArrayList<>();
    private LinearLayout li_error_msg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        mcontext = this;
        Utils.showTitle1(window, mcontext);
        setContentView(R.layout.addresslist_layout);
        Log.e("AddressListActivity", "AddressListActivity");
        preferences = getSharedPreferences(mcontext.getString(R.string.app_name), Context.MODE_PRIVATE);
        init();
    }

    public void init() {
        progressDialog = new ProgressDialog(mcontext, R.style.AppCompatAlertDialogStyle);
        recycle_addressList = findViewById(R.id.recycle_addressList);
        linearLayoutManager = new LinearLayoutManager(AddressListActivity.this, RecyclerView.VERTICAL, false);
        recycle_addressList.setLayoutManager(linearLayoutManager);
        txt_add = findViewById(R.id.txt_add);
        img_back = findViewById(R.id.img_back);
        li_error_msg = findViewById(R.id.li_error_msg);
        txt_add.setOnClickListener(this);
        img_back.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_add:
                AppConstant.address_id = "";
                AppConstant.chk_edt = "";
                Intent i = new Intent(AddressListActivity.this, AddAdressActivity.class);
                startActivity(i);
                break;
            case R.id.img_back:
                finish();
                break;

        }
    }

    private void myaddresslist() {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.myaddresslist, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    array_addresslist.clear();
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        li_error_msg.setVisibility(View.GONE);
                        JSONObject obj_result = json_main.optJSONObject("result");
                        JSONArray array_address_info = obj_result.optJSONArray("user_address");
                        for (int i = 0; i < array_address_info.length(); i++) {
                            AddressModel addressModel = new AddressModel();
                            addressModel.setShipingId(array_address_info.optJSONObject(i).optString("id"));
                            addressModel.setAddress_type(array_address_info.optJSONObject(i).optString("address_type"));
                            addressModel.setCity(array_address_info.optJSONObject(i).optString("city"));
                            addressModel.setAddressLine1(array_address_info.optJSONObject(i).optString("address_line1"));
                            addressModel.setAddressLine2(array_address_info.optJSONObject(i).optString("address_line2"));
                            addressModel.setState(array_address_info.optJSONObject(i).optString("state"));
                            addressModel.setZip(array_address_info.optJSONObject(i).optString("pincode"));
                            array_addresslist.add(addressModel);
                        }
                        if (array_addresslist.size() > 0) {
                            li_error_msg.setVisibility(View.GONE);
                            recycle_addressList.setVisibility(View.VISIBLE);
                            addressListAdpter = new AddressListAdpter(AddressListActivity.this, array_addresslist);
                            recycle_addressList.setAdapter(addressListAdpter);
                            addressListAdpter.notifyDataSetChanged();
                        } else {
                            recycle_addressList.setVisibility(View.GONE);
                            li_error_msg.setVisibility(View.VISIBLE);
                        }

                    } else {
                        recycle_addressList.setVisibility(View.GONE);
                        li_error_msg.setVisibility(View.VISIBLE);
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                Log.e("params", "" + Constants.myaddresslist + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.myaddresslist);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class AddressListAdpter extends RecyclerView.Adapter<AddressListAdpter.MyViewHolder> {
        private List<AddressModel> dataList;
        private Context context;
        int pos = 0;

        public AddressListAdpter(Context context, List<AddressModel> listClothingPrice) {
            this.context = context;
            this.dataList = listClothingPrice;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_adpter_layout, parent, false);
            return new AddressListAdpter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull final AddressListAdpter.MyViewHolder holder, final int position) {
            final AddressModel model = dataList.get(position);
            holder.img_delete.setEnabled(true);
            if (model.getAddressLine1().equals("") || model.getAddressLine2().equals("") || model.getCity().equals("")) {

            } else {
                holder.txt_address1.setText(model.getAddress_type() + " : " + model.getAddressLine1() + "\n" + model.getAddressLine2() + "\n" + model.getCity() + "\n" + model.getState() + "\n" + model.getZip());
            }

            if (AppConstant.pos == position) {
                holder.img_Unselect.setBackgroundResource(R.drawable.tick_button);
            } else {
                holder.img_Unselect.setBackgroundResource(R.drawable.cheched);
            }

            holder.img_Unselect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstant.pos = position;
                    SharedPreferences pre = getSharedPreferences("ShipingAddress", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pre.edit();
                    editor.putString("ShipingId", model.getShipingId());
                    editor.commit();
                    AppConstant.address_id = model.getShipingId();
                    AppConstant.Addressline1 = model.getAddressLine1();
                    AppConstant.Addressline2 = model.getAddressLine2();
                    AppConstant.city = model.getCity();
                    AppConstant.zip = model.getZip();
                    AppConstant.state_address = model.getState();
                    AppConstant.address_type = model.getAddress_type();
                    holder.img_Unselect.setBackgroundResource(R.drawable.tick_button);
                    AppConstant.chk_edt = "";
                    startActivity(new Intent(AddressListActivity.this, DeliveryAddressActivity.class));
                }
            });

            holder.img_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences pre = getSharedPreferences("ShipingAddress", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pre.edit();
                    editor.putString("ShipingId", model.getShipingId());
                    editor.commit();

                    AppConstant.chk_edt = "edit";
                    AppConstant.address_id = model.getShipingId();
                    AppConstant.Addressline1 = model.getAddressLine1();
                    AppConstant.Addressline2 = model.getAddressLine2();
                    AppConstant.city = model.getCity();
                    AppConstant.zip = model.getZip();
                    AppConstant.state_address = model.getState();
                    AppConstant.address_type = model.getAddress_type();
                    startActivity(new Intent(AddressListActivity.this, AddAdressActivity.class));
                }
            });

            holder.img_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddressListActivity.this, R.style.AppCompatAlertDialogStyle);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("Do you want to delete this address?");
                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            deleteaddress(model.getShipingId());
                        }
                    });
                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            AppConstant.check_cart_view = "";
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface arg0) {
                            alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                            alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                            //dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.black));
                        }
                    });
                    alert.show();
                }
            });
            holder.card_main1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppConstant.check_cart.equals("card_screen")) {
                        SharedPreferences pre = getSharedPreferences("ShipingAddress", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pre.edit();
                        editor.putString("ShipingId", model.getShipingId());
                        editor.commit();
                        AppConstant.address_id = model.getShipingId();
                        AppConstant.Addressline1 = model.getAddressLine1();
                        AppConstant.Addressline2 = model.getAddressLine2();
                        AppConstant.city = model.getCity();
                        AppConstant.zip = model.getZip();
                        AppConstant.state_address = model.getState();
                        AppConstant.address_type = model.getAddress_type();
                        startActivity(new Intent(AddressListActivity.this, DeliveryAddressActivity.class));
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return dataList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView txt_address1, txt_address2, txt_address3;
            private ImageView img_Unselect, img_edit, img_delete;
            private CardView card_main1;

            public MyViewHolder(View view) {
                super(view);
                txt_address1 = (TextView) view.findViewById(R.id.txt_address1);
                img_Unselect = (ImageView) view.findViewById(R.id.img_Unselect);
                img_edit = (ImageView) view.findViewById(R.id.img_edit);
                img_delete = (ImageView) view.findViewById(R.id.img_delete);
                card_main1 = (CardView) view.findViewById(R.id.card_main1);
            }
        }
    }

    private void deleteaddress(String address_id) {
        String tag_string_req = "req";
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        final StringRequest strReq = new StringRequest(Request.Method.POST, Constants.deleteaddress, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.e("response", "" + response);
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                try {
                    JSONObject json_main = new JSONObject(response);
                    if (json_main.optString("status").equalsIgnoreCase("1")) {
                        myaddresslist();
                    } else {
                        Toast.makeText(mcontext, json_main.optString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressDialog.dismiss();
                }
            }

        }, new Response.ErrorListener() {
            public void onErrorResponse(VolleyError error) {
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", preferences.getString(PreferenceManager.user_id, ""));
                params.put("address_id", address_id);
                Log.e("params", "" + Constants.deleteaddress + params);
                return params;
            }
        };
        strReq.setRetryPolicy(new DefaultRetryPolicy(100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().getRequestQueue().getCache().remove(Constants.deleteaddress);
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onResume() {
        if (Utils.isNetworkAvailable(AddressListActivity.this)) {
            myaddresslist();
        } else {
            Toast.makeText(mcontext, R.string.check_internet, Toast.LENGTH_LONG).show();
        }
        super.onResume();
    }
}
