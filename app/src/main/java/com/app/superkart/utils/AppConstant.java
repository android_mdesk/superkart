package com.app.superkart.utils;

import org.json.JSONArray;

public class AppConstant {

    public static final int PLACE_ORDER_HOME = 301;
    public static final int STORAGE_PERMISSION_REQUEST_CODE = 200;
    public static final int CAMERA_PERMISSION_REQUEST_CODE = 300;
    public static String check_sign_up = "";
    public static String order_id = "";
    public static String amount = "";
    public static String note = "SuperKart365 Order";
    public static String secretKey = "";
    public static String appId = "";
    public static String payment_mode_cashfree_url = "";
    public static String check_without_social = "";
    public static String check_calender_view = "";
    public static String Current_Location = "0";
    public static String scan_open = "0";
    public static String check_flow = "";
    public static String scannning_url = "";
    public static String scannning_view = "";
    public static String qr_code_login = "0";
    public static String state = "";
    public static String click_main_module = "";
    public static Integer pos = 0;
    public static String str_Address = "";
    public static String chk_edt = "";
    public static String Addressline1 = "";
    public static String Addressline2 = "";
    public static String phone = "";
    public static String name = "";
    public static String email = "";
    public static String city = "";
    public static String state_address = "";
    public static String zip = "";
    public static String address_type = "";
    public static String address_id = "";
    public static String total_price = "";
    public static String check_cart_view = "";
    public static String check_cart = "";
    public static String order_id_deatil = "";
    public static String chk_payment_view = "";
    public static String vender_id = "";
    public static String LoyaltyPoints = "";
    public static String seller_id = "";
    public static String coupon_id = "";
    public static JSONArray jsonArray;

}
