package com.app.superkart.model.Shopping;

public class HomeCategoryModel {
    int imgg_category;
    String item_name,category_id,cate_image,color_code,category_slider,offer_arr,state,state_id;

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getOffer_arr() {
        return offer_arr;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setOffer_arr(String offer_arr) {
        this.offer_arr = offer_arr;
    }

    public String getCategory_slider() {
        return category_slider;
    }

    public void setCategory_slider(String category_slider) {
        this.category_slider = category_slider;
    }

    public String getColor_code() {
        return color_code;
    }

    public void setColor_code(String color_code) {
        this.color_code = color_code;
    }

    public String getCate_image() {
        return cate_image;
    }

    public void setCate_image(String cate_image) {
        this.cate_image = cate_image;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public HomeCategoryModel(int imgg_category, String item_name) {
        this.imgg_category = imgg_category;
        this.item_name = item_name;
    }



    public HomeCategoryModel(int imgg_category) {
        this.imgg_category = imgg_category;
    }

    public HomeCategoryModel() {

    }

    public int getImgg_category() {
        return imgg_category;
    }

    public void setImgg_category(int imgg_category) {
        this.imgg_category = imgg_category;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }
}
