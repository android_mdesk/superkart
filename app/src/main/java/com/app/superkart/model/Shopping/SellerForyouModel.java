package com.app.superkart.model.Shopping;

public class SellerForyouModel {
    int imgg_category, rating;
    String storeName, km;
    String seller_id, seller_name, seller_image, seller_state, seller_city, time, product_image, product_id, product_name,item_array;
    String storename,selling_type,item_type;

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getSelling_type() {
        return selling_type;
    }

    public void setSelling_type(String selling_type) {
        this.selling_type = selling_type;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getItem_array() {
        return item_array;
    }

    public void setItem_array(String item_array) {
        this.item_array = item_array;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public SellerForyouModel() {

    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSeller_id() {
        return seller_id;
    }

    public void setSeller_id(String seller_id) {
        this.seller_id = seller_id;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getSeller_image() {
        return seller_image;
    }

    public void setSeller_image(String seller_image) {
        this.seller_image = seller_image;
    }

    public String getSeller_state() {
        return seller_state;
    }

    public void setSeller_state(String seller_state) {
        this.seller_state = seller_state;
    }

    public String getSeller_city() {
        return seller_city;
    }

    public void setSeller_city(String seller_city) {
        this.seller_city = seller_city;
    }

    public SellerForyouModel(int imgg_category, String storeName, String km) {
        this.imgg_category = imgg_category;
        this.storeName = storeName;
        this.km = km;
    }


    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }


    public int getImgg_category() {
        return imgg_category;
    }

    public void setImgg_category(int imgg_category) {
        this.imgg_category = imgg_category;
    }
}

