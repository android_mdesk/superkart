package com.app.superkart.model.Shopping;

public class ShipModel {
    String status, tracking_no, array_shippment_data, order_status, shippment_status, date, time, msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTracking_no() {
        return tracking_no;
    }

    public void setTracking_no(String tracking_no) {
        this.tracking_no = tracking_no;
    }

    public String getArray_shippment_data() {
        return array_shippment_data;
    }

    public void setArray_shippment_data(String array_shippment_data) {
        this.array_shippment_data = array_shippment_data;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getShippment_status() {
        return shippment_status;
    }

    public void setShippment_status(String shippment_status) {
        this.shippment_status = shippment_status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
