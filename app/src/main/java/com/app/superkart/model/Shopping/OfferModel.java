package com.app.superkart.model.Shopping;

public class OfferModel {
    int img;

    public OfferModel() {

    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    String offer_title, offer_type, offer_item, product_id, product_name, product_image;
    String coupon_id, coupon_code, Discount_Type, Discount_Price, coupon_image,offer_id;
    String storename,selling_type,item_type,seller_name;

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getSelling_type() {
        return selling_type;
    }

    public void setSelling_type(String selling_type) {
        this.selling_type = selling_type;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }

    public String getSeller_name() {
        return seller_name;
    }

    public void setSeller_name(String seller_name) {
        this.seller_name = seller_name;
    }

    public String getOffer_id() {
        return offer_id;
    }

    public void setOffer_id(String offer_id) {
        this.offer_id = offer_id;
    }

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public void setCoupon_code(String coupon_code) {
        this.coupon_code = coupon_code;
    }

    public String getDiscount_Type() {
        return Discount_Type;
    }

    public void setDiscount_Type(String discount_Type) {
        Discount_Type = discount_Type;
    }

    public String getDiscount_Price() {
        return Discount_Price;
    }

    public void setDiscount_Price(String discount_Price) {
        Discount_Price = discount_Price;
    }

    public String getCoupon_image() {
        return coupon_image;
    }

    public void setCoupon_image(String coupon_image) {
        this.coupon_image = coupon_image;
    }

    public OfferModel(int product_image, String item_name) {
        this.img = product_image;
        this.offer_item = item_name;
    }

    public String getOffer_title() {
        return offer_title;
    }

    public void setOffer_title(String offer_title) {
        this.offer_title = offer_title;
    }

    public String getOffer_type() {
        return offer_type;
    }

    public void setOffer_type(String offer_type) {
        this.offer_type = offer_type;
    }

    public String getOffer_item() {
        return offer_item;
    }

    public void setOffer_item(String offer_item) {
        this.offer_item = offer_item;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }
}
