package com.app.superkart.model.Service;

public class ServiceModel {
    int img;
    String id, name;

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public ServiceModel(String name) {
        this.name = name;
    }

    public ServiceModel(int img, String name) {
        this.img = img;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
