package com.app.superkart.model.Shopping;

public class LoyaltyModel {
   String vid,image,sellername,loyalitypoint;

    public String getVid() {
        return vid;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSellername() {
        return sellername;
    }

    public void setSellername(String sellername) {
        this.sellername = sellername;
    }

    public String getLoyalitypoint() {
        return loyalitypoint;
    }

    public void setLoyalitypoint(String loyalitypoint) {
        this.loyalitypoint = loyalitypoint;
    }
}
