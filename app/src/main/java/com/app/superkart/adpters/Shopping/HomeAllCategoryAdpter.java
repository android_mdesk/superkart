package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.Shopping.Home_ViewAll_Category_List;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeAllCategoryAdpter extends RecyclerView.Adapter<HomeAllCategoryAdpter.MyViewHolder> {

    private List<HomeCategoryModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;
        CardView card_main1;


        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public HomeAllCategoryAdpter(Context context, List<HomeCategoryModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public HomeAllCategoryAdpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_all_category, parent, false);

        return new HomeAllCategoryAdpter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeAllCategoryAdpter.MyViewHolder holder, int position) {
        HomeCategoryModel model = arrayList.get(position);

        if (model.getItem_name().equals("")) {

        } else {
            holder.txt_item.setText(model.getItem_name());
        }

        if (model.getCate_image().equals("")) {

        } else {
           /* Picasso.with(context)
                    .load(model.getCate_image())
                    .into(holder.img_item);*/

            Glide
                    .with(context)
                    .load(model.getCate_image())
                    .centerCrop()
                    .into(holder.img_item);
        }


        if (pos == position) {
            holder.li_bg.setBackgroundResource(R.drawable.border_blue);
            holder.txt_item.setTextColor(Color.parseColor("#1576CE"));
        } else {
            holder.li_bg.setBackgroundResource(R.drawable.border_gray);
            holder.txt_item.setTextColor(Color.parseColor("#FF000000"));
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();
                Intent i = new Intent(context, Home_ViewAll_Category_List.class);
                i.putExtra("category_id",model.getCategory_id());
                context.startActivity(i);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

