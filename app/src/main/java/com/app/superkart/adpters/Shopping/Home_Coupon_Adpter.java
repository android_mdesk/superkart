package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.model.Shopping.OfferModel;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Home_Coupon_Adpter extends RecyclerView.Adapter<Home_Coupon_Adpter.MyViewHolder> {

    private List<OfferModel> arrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public Home_Coupon_Adpter(Context context, List<OfferModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_coupon_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        OfferModel model = arrayList.get(position);

       /* Picasso.with(context)
                .load(model.getCoupon_image())
                .into(holder.img_item);*/

        if (model.getCoupon_image().equals("") | model.getCoupon_image().equals("null") || model.getCoupon_image().equals(null)) {

        } else {
            Glide
                    .with(context)
                    .load(model.getCoupon_image())
                    .centerCrop()
                    .into(holder.img_item);
        }

        if (model.getCoupon_code().equals("") | model.getCoupon_code().equals("null") || model.getCoupon_code().equals(null)) {

        } else {
            holder.txt_item.setText(model.getCoupon_code());
        }


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}



