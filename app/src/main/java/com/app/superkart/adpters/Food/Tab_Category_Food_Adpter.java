package com.app.superkart.adpters.Food;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Tab_Category_Food_Adpter extends RecyclerView.Adapter<Tab_Category_Food_Adpter.MyViewHolder> {

    private List<HomeCategoryModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;


        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public Tab_Category_Food_Adpter(Context context, List<HomeCategoryModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public Tab_Category_Food_Adpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_categoty_tab_food, parent, false);

        return new Tab_Category_Food_Adpter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Tab_Category_Food_Adpter.MyViewHolder holder, int position) {
        HomeCategoryModel model = arrayList.get(position);


        Picasso.with(context)
                .load(model.getImgg_category())
                .into(holder.img_item);
        holder.txt_item.setText(model.getItem_name());

        if (pos == position) {
            holder.li_bg.setBackgroundResource(R.drawable.border_blue);
            holder.txt_item.setTextColor(Color.parseColor("#1576CE"));
        } else {
            holder.li_bg.setBackgroundResource(R.drawable.border_gray);
            holder.txt_item.setTextColor(Color.parseColor("#FF000000"));
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}



