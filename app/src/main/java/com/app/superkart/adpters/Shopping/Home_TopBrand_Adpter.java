package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.BrandModel;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Home_TopBrand_Adpter extends RecyclerView.Adapter<Home_TopBrand_Adpter.MyViewHolder> {

    private List<BrandModel> arrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public Home_TopBrand_Adpter(Context context, List<BrandModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_topbrand_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BrandModel model = arrayList.get(position);

       /* Picasso.with(context)
                .load(model.getBrand_image())
                .into(holder.img_item);*/

        Glide
                .with(context)
                .load(model.getBrand_image())
                .centerCrop()
                .into(holder.img_item);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}




