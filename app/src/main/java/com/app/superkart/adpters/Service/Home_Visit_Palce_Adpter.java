package com.app.superkart.adpters.Service;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Service.ServiceModel;
import com.app.superkart.ui.activity.Service.Service_Detail_Option_Activity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Home_Visit_Palce_Adpter extends RecyclerView.Adapter<Home_Visit_Palce_Adpter.MyViewHolder> {

    private List<ServiceModel> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_storeName, txt_Add, tv_viewDetails;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_storeName = view.findViewById(R.id.txt_storeName);
            txt_Add = view.findViewById(R.id.txt_Add);
            li_bg = view.findViewById(R.id.li_bg);
            tv_viewDetails = view.findViewById(R.id.tv_viewDetails);

        }
    }

    public Home_Visit_Palce_Adpter(Context context, List<ServiceModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public Home_Visit_Palce_Adpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.place_order_home_visit_adpter, parent, false);

        return new Home_Visit_Palce_Adpter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Home_Visit_Palce_Adpter.MyViewHolder holder, int position) {
        ServiceModel model = arrayList.get(position);

        Picasso.with(context)
                .load(model.getImg())
                .into(holder.img_item);
        holder.txt_storeName.setText(model.getName());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
