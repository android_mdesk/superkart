package com.app.superkart.adpters.Service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Service.TimeModel;
import com.app.superkart.utils.AppConstant;

import java.util.List;


public class TimeSlotAdpter extends RecyclerView.Adapter<TimeSlotAdpter.MyViewHolder> {


    private Context context;
    private List<TimeModel> dataList;
    private int centerPosition = 0;
    // private onSetMonthNameListener listener;

    public TimeSlotAdpter(Context context, List<TimeModel> dates) {
        this.context = context;
        this.dataList = dates;
        // this.listener = onSetMonthNameListener;

    }

    @NonNull
    @Override
    public TimeSlotAdpter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_horizontal_calander, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        TimeModel timeModel = dataList.get(position);
        holder.txt_date.setText(timeModel.getTime());
        holder.txt_day.setText(timeModel.getName());
        if (centerPosition == position) {
            if (AppConstant.check_calender_view.equals("food")) {
                holder.ll_parent.setBackgroundResource(R.drawable.round_yellow);
            } else {
                holder.ll_parent.setBackgroundResource(R.drawable.round_bg_blue);
            }

            holder.txt_date.setTextColor(Color.parseColor("#FFFFFFFF"));
            holder.txt_day.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.ll_parent.setBackgroundResource(R.drawable.round_gray_border);
            holder.txt_date.setTextColor(Color.parseColor("#bdbdbd"));
            holder.txt_day.setTextColor(Color.parseColor("#bdbdbd"));
        }


        holder.ll_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                centerPosition = position;
                notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView txt_date;
        private final TextView txt_day;
        private final LinearLayout ll_parent;

        public MyViewHolder(View view) {
            super(view);
            ll_parent = (LinearLayout) view.findViewById(R.id.ll_parent);
            txt_date = view.findViewById(R.id.txt_date);
            txt_day = view.findViewById(R.id.txt_day);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

