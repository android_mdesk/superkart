package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;

import java.util.List;

public class Add_To_Bag_Adpter extends RecyclerView.Adapter<Add_To_Bag_Adpter.MyViewHolder> {

    private List<String> arrayList;
    private Context context;
    String[] Array_Size, Array_Qty;
    ArrayAdapter<String> adptSize, adptQty;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        Spinner spinner_size, spinner_Qty;

        public MyViewHolder(View view) {

            super(view);
            spinner_size = view.findViewById(R.id.spinner_size);
            spinner_Qty = view.findViewById(R.id.spinner_Qty);
        }
    }

    public Add_To_Bag_Adpter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public Add_To_Bag_Adpter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adpter_add_to_bag_layout, parent, false);

        return new Add_To_Bag_Adpter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(Add_To_Bag_Adpter.MyViewHolder holder, int position) {

        Array_Size = context.getResources().getStringArray(R.array.array_size);
        adptSize = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Array_Size);
        adptSize.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spinner_size.setAdapter(adptSize);
        holder.spinner_size.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ((TextView) view).setTextColor(Color.BLACK);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }

        });

        Array_Qty = context.getResources().getStringArray(R.array.array_qty);
        adptQty = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Array_Qty);
        adptQty.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.spinner_Qty.setAdapter(adptQty);
        holder.spinner_Qty.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                ((TextView) view).setTextColor(Color.BLACK);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }

        });


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}
