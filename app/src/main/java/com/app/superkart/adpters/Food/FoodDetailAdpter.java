package com.app.superkart.adpters.Food;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.ui.activity.Food.FoodDetail_1Activity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FoodDetailAdpter extends RecyclerView.Adapter<FoodDetailAdpter.MyViewHolder> {

    private List<String> arrayList;
    private Context context;
    int pos = 0;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_storeName, txt_Add, tv_viewDetails;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_storeName = view.findViewById(R.id.txt_storeName);
            txt_Add = view.findViewById(R.id.txt_Add);
            li_bg = view.findViewById(R.id.li_bg);
            tv_viewDetails = view.findViewById(R.id.tv_viewDetails);


        }
    }

    public FoodDetailAdpter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_detail_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        if (pos == position) {
            holder.txt_Add.setBackgroundResource(R.drawable.box_corner_blue);
            holder.txt_Add.setTextColor(Color.parseColor("#FFFFFFFF"));
        } else {
            holder.txt_Add.setBackgroundResource(R.drawable.border_blue_new);
            holder.txt_Add.setTextColor(Color.parseColor("#1576CE"));
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pos = position;
                notifyDataSetChanged();
                Intent i = new Intent(context, FoodDetail_1Activity.class);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}

