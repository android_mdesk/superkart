package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.superkart.R;

import com.bumptech.glide.Glide;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home_Fashion_BannerAdpter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    ArrayList<String> listimage;


    public Home_Fashion_BannerAdpter(FragmentActivity activity, ArrayList<String> listimage) {
        this.mContext = activity;
        this.listimage = listimage;
    }

    @Override
    public int getCount() {
        return listimage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.view_category_fashion_adpter, null);
        ImageView imgPhoto = (ImageView) view.findViewById(R.id.image);
        String imageload = listimage.get(position).toString();

        /*Picasso.with(mContext)
                .load(imageload)
                .into(imgPhoto);*/

        if (imageload.equals("") || imageload.equals("null") || imageload.equals(null)) {

        } else {
            Glide
                    .with(mContext)
                    .load(imageload)
                    .centerCrop()
                    .into(imgPhoto);
            ((ViewPager) container).addView(view);

        }


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);

    }
  
}
