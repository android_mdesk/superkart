package com.app.superkart.adpters.Shopping;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.superkart.R;
import com.app.superkart.model.Shopping.HomeCategoryModel;
import com.app.superkart.model.Shopping.SellerForyouModel;
import com.app.superkart.ui.activity.Shopping.Home_View_Fashion_Detail;
import com.app.superkart.ui.activity.Shopping.SellerForyouDetailActivity;
import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Home_sellerforU_Adpter extends RecyclerView.Adapter<Home_sellerforU_Adpter.MyViewHolder> {

    private List<SellerForyouModel> arrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img_item;
        public TextView txt_item;
        LinearLayout li_bg;

        public MyViewHolder(View view) {
            super(view);
            img_item = view.findViewById(R.id.img_item);
            txt_item = view.findViewById(R.id.txt_item);
            li_bg = view.findViewById(R.id.li_bg);

        }
    }

    public Home_sellerforU_Adpter(Context context, List<SellerForyouModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_seller_adpter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SellerForyouModel model = arrayList.get(position);

        if (model.getSeller_image().equals("")) {

        } else {
           /* Picasso.with(context)
                    .load(model.getSeller_image())
                    .into(holder.img_item);
*/
            Glide
                    .with(context)
                    .load(model.getSeller_image())
                    .centerCrop()
                    .into(holder.img_item);
        }

        if (model.getSeller_name().equals("")) {

        } else {
            holder.txt_item.setText(model.getSeller_name());
        }

        holder.li_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, SellerForyouDetailActivity.class);
                i.putExtra("seller_id", model.getSeller_id());
                i.putExtra("image", model.getSeller_image());
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }
}




